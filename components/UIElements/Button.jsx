const Button = {
  baseStyle: {
    fontWeight: 'bold',
    textTransform: 'uppercase',
    borderRadius: 'base',
    textTransform: 'none',
  },
  sizes: {
    sm: {
      fontSize: 'sm',
      px: 4,
      py: 3,
    },
    md: {
      fontSize: 'md',
    },
  },
  variants: {
    outline: {
      border: '2px solid',
      borderColor: 'black',
      color: 'black',
    },
    solid: {
      bg: 'black',
      color: 'white',
      _hover: {
        bg: 'black',
      },
      _active: {
        bg: 'black',
      },
      _focus: {
        bg: 'black',
      },
    },
  },
  defaultProps: {
    size: 'md',
    variant: 'solid',
  },
};

export default Button;
