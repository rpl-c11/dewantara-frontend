import Link from 'next/dist/client/link';
import { ButtonAuth } from './Styles';

export default function NotAuthenticated() {
  return (
    <>
      <li className="nav-item ms-auto my-lg-0 my-1">
        <Link href="/login/">
          <ButtonAuth
            variant="outline"
            size="md"
            width="80px"
            borderColor="#000"
          >
            Log In
          </ButtonAuth>
        </Link>
      </li>
      <li className="nav-item ms-auto my-lg-0 my-1">
        <Link href="/signup/">
          <ButtonAuth
            variant="ghost"
            colorScheme="Black & White"
            size="md"
            width="80px"
            color="#fff"
            background="#000"
          >
            Sign Up
          </ButtonAuth>
        </Link>
      </li>
    </>
  );
}
