import {
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/modal';
import {
  Flex,
  Button,
  Menu,
  MenuList,
  MenuItem,
  MenuButton,
  Link as ChakraLink,
} from '@chakra-ui/react';
import Link from 'next/link';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useDisclosure } from '@chakra-ui/hooks';

import { deleteForumDetail } from '../../services/forum';
import forum_styles from '../../styles/courseitems/Forum.module.css';
import assignment_styles from '../../styles/courseitems/Assignment.module.css';

export default function ForumItem({ name, isEdit, id }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();

  const deleteForum = async () => {
    await deleteForumDetail(id);
    router.reload();
  };

  return (
    <Flex justifyContent="space-between">
      <Link href={`/forum/${id}`}>
        <div className={forum_styles.forumcontainer}>
          <Image
            width={20}
            height={20}
            src="https://scele.cs.ui.ac.id/theme/image.php/lambda/forum/1636195207/icon"
            alt="forum-icon"
            className={forum_styles.img}
          />
          <span>{name}</span>
        </div>
      </Link>

      {isEdit && (
        <>
          <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Yakin ingin menghapus {name}?</ModalHeader>
              <ModalFooter>
                <Button onClick={deleteForum} mr="10px" colorScheme="red">
                  Delete
                </Button>
                <Button colorScheme="blue" mr={3} onClick={onClose}>
                  Close
                </Button>
              </ModalFooter>
            </ModalContent>
          </Modal>

          <Menu size="sm" border="none" placeholder="edit">
            <MenuButton>
              Edit <b className={assignment_styles.caret}></b>
            </MenuButton>
            <MenuList>
              <MenuItem>
                <Link href={`/forum/${id}/edit`}>Edit</Link>
              </MenuItem>
              <MenuItem>
                <ChakraLink onClick={onOpen}>Delete</ChakraLink>
              </MenuItem>
            </MenuList>
          </Menu>
        </>
      )}
    </Flex>
  );
}
