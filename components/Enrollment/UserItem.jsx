import forum_styles from '../../styles/courseitems/Forum.module.css';
import Link from 'next/dist/client/link';
import { Table, Thead, Tr, Th, Tbody, Td, Text, Flex } from '@chakra-ui/react';
import { Button } from "@chakra-ui/button";
export default function UserItem({ name, role, isTeacher, id, onDelete , onUbah}) {


  return (
    <Tr>
      <Td>{name}</Td>
      <Td>{role === 1 ? "Teacher" : "Student"}</Td>
      {isTeacher && 
      <>
        <Td>
            <Button
            variant="outline"
            size="md"
            width="80px"
            borderColor="blue"
            onClick={() => onUbah(id)}
            >
            Ubah
            </Button>
        
        </Td>
        <Td>
            <Button
            variant="outline"
            size="md"
            width="80px"
            borderColor="red"
            onClick={() => onDelete(id)}>
            Hapus
            </Button>
       
        </Td>
      </>
      }
        
    </Tr>
  );
}
