import { MenuItem } from '@chakra-ui/menu';
import Link from 'next/dist/client/link';

export default function CourseButton({ id, children }) {
  return (
    <MenuItem>
      <Link href={`/course/${id}`}>{children}</Link>
    </MenuItem>
  );
}
