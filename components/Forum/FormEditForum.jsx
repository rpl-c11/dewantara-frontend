import {
  FormControl,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/form-control';
import { Button, Input, useToast, Heading } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { updateForumDetail } from '../../services/forum';
import { useRouter } from 'next/router';

const FormEditForum = ({ forumId, judul, forumDetail }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm({ defaultValues: forumDetail });
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await updateForumDetail(forumId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil mengedit forum!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.push(`/forum/${forumId}`);
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengedit forum',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
        <Heading fontSize="1.25rem" mt="1rem">
          {forumDetail?.name}
        </Heading>
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={!!errors.name} id="judul" isRequired my={5}>
            <FormLabel>Judul</FormLabel>
            <Input
              type="text"
              {...register('name', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.name && errors.name.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Forum
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormEditForum;
