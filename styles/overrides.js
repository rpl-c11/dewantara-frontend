export const overrideButtonVariantToRed = {
  _hover: { bg: 'red' },
  _focus: { bg: 'red' },
  _active: { bg: 'red' },
  variant: 'solid',
  bg: 'red',
};
