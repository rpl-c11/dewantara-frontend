import { useRouter } from 'next/dist/client/router';
import Head from 'next/head';
import FormLogin from '../components/FormLogin/FormLogin';
import Layout from '../components/Layout';
import styles from '../styles/Home.module.css';

export default function Login() {
  var isLogin = false;
  const router = useRouter();

  if (typeof window !== 'undefined') {
    if (localStorage.getItem('role')) {
      isLogin = true;
    }
    if (isLogin) {
      router.push('/');
    }
  }

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Log In</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormLogin />
      </div>
    </Layout>
  );
}
