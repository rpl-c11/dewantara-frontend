import { useEffect, useState } from 'react';
import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { getThread } from '../../../../../services/forum';
import FormEditThread from '../../../../../components/Forum/FormEditThread';
import Layout from '../../../../../components/Layout';
import styles from '/styles/Home.module.css';

export default function EditThread() {
  const router = useRouter();
  const toast = useToast();
  const [forumId, setForumId] = useState(null);
  const [threadId, setThreadId] = useState(null);
  const [threadDetail, setThreadDetail] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { forumId: routerForumId, threadId: routerThreadId } = router.query;
      setForumId(routerForumId);
      setThreadId(routerThreadId);
      fetchThreadDetail(routerForumId, routerThreadId);
    }
  }, [router.isReady]);

  const fetchThreadDetail = async (forumId, threadId) => {
    try {
      const threadDetailData = await getThread(forumId, threadId);
      const { title, body } = await threadDetailData.data;
      setThreadDetail({ title, body });
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data thread',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Edit Thread | {threadDetail?.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {threadDetail && (
          <FormEditThread
            forumId={forumId}
            threadId={threadId}
            threadDetail={threadDetail}
            judul="Edit Thread"
          />
        )}
      </div>
    </Layout>
  );
}
