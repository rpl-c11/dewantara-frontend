import styled from 'styled-components';
import { Box } from '@chakra-ui/layout';

const Judul = styled(Box)`
  font-weight: bold;
  font-size: 0.9rem;
`;
const DivKiri = styled.div`
  width: 50px;
  flex-shrink: 0;
  border-radius: 50%;
  overflow: hidden;
`;
const DivKanan = styled.div`
  flex-grow: 1;
`;

export { Judul, DivKiri, DivKanan };
