import { Button } from '@chakra-ui/button';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Flex } from '@chakra-ui/layout';
import { Textarea } from '@chakra-ui/textarea';
import LoginBox, { OuterBox } from './Styles';
import { useRouter } from 'next/dist/client/router';
import axiosInstance from '../../services/axios';
import { useState } from 'react';
import Swal from 'sweetalert2';

const FormPengumuman = (props) => {
  const router = useRouter();
  const [announcementData, setAnnouncementData] = useState({
    title: '',
    content: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAnnouncementData({
      ...announcementData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axiosInstance
      .post(`announcement/`, announcementData)
      .then((res) => {
        Swal.fire({
          title: 'Success!',
          text: 'Berhasil Menambahkan Pengumuman!',
          icon: 'success',
          confirmButtonText: 'OK',
        }).then(router.push('/'));
      })
      .catch((err) => console.log(err));
  };

  return (
    <OuterBox>
      <h2
        style={{ fontSize: '1.5rem', fontWeight: 'bold', textAlign: 'center' }}
      >
        {props.judul}
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit}>
          <FormControl id="judul" my={5} isRequired>
            <FormLabel>Judul</FormLabel>
            <Input name="title" type="text" onChange={handleChange} />
          </FormControl>
          <FormControl id="isi" my={5} isRequired>
            <FormLabel>Isi Pengumuman</FormLabel>
            <Textarea name="content" rows="10" onChange={handleChange} />
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
            >
              Simpan Pengumuman
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormPengumuman;
