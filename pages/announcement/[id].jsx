import Layout from '../../components/Layout';
import Announcement from '../../components/Announcement/Announcement';
import { Flex } from '@chakra-ui/layout';
import announcement_styles from '../../styles/Announcement.module.css';
import Head from 'next/dist/shared/lib/head';
import { axioss } from '../../services/axios';

// export const getStaticPaths = async () => {
//   var data = {};
//   await axioss
//     .get(`announcement/`)
//     .then((res) => {
//       const paths = res.data.map((announcement) => {
//         return {
//           params: {id: announcement.id.toString()} ,
//         };
//       });
//       data['paths'] = paths;
//     })
//     .catch((err) => {
//       console.log(err);
//       return null;
//     });
//   data['fallback'] = true;
//   return data;
// };

export const getServerSideProps = async (context) => {
  const id = context.params.id;
  var data = null;
  await axioss
    .get(`announcement/${id}/`)
    .then((res) => {
      data = {
        props: { announcement: res.data },
      };
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return data;
};

export default function getAnnouncement({ announcement }) {
  return (
    <Layout>
      <Head>
        <title>Pengumuman</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Flex justify="center" direction="column" align="center" minHeight="80vh">
        <div className={announcement_styles.announcementcontainer}>
          <Announcement
            key={announcement.id}
            id={announcement.id}
            judul={announcement.title}
            date={announcement.created_at}
            author={announcement.created_by.name}
            username={announcement.created_by.username}
            content={announcement.content}
          ></Announcement>
        </div>
      </Flex>
    </Layout>
  );
}
