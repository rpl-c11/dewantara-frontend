import { Button } from '@chakra-ui/button';
import { useDisclosure } from '@chakra-ui/hooks';
import { Center, Flex, Link, Text } from '@chakra-ui/layout';
import { Menu, MenuButton, MenuItem, MenuList } from '@chakra-ui/menu';
import {
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/modal';
import { Table, Tbody, Td, Tr } from '@chakra-ui/table';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import Layout from '../../../components/Layout';
import axiosInstance from '../../../services/axios';
import detailassignment_styles from '../../../styles/assignments/DetailAssignment.module.css';
import assignment_styles from '../../../styles/courseitems/Assignment.module.css';

export default function Assignment() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const { id } = router.query;

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [duedate, setduedate] = useState('');

  const [submissionStatus, setSubmissionStatus] = useState('No attempt');
  const [gradingStatus, setGradingStatus] = useState('Not graded');
  const [timeRemaining, setTimeRemaining] = useState('-');
  const [lastModified, setLastModified] = useState('-');
  const [grade, setGrade] = useState('-');
  const [isLate, setIsLate] = useState(true);
  const [section, setSection] = useState();
  const [isTeacher, setIsTeacher] = useState(true);
  const [courseId, setCourseId] = useState();

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      router.push('/login');
    }
  }, []);

  async function deleteAssignment() {
    await axiosInstance.delete(`/assignment/${id}/`);
    router.push(`/course/${courseId}`);
  }

  useEffect(async () => {
    if (id) {
      const data = (await axiosInstance.get(`assignment/${id}/`)).data;
      setTitle(data.name);
      setDescription(data.description);
      setduedate(new Date(data.due_date).toDateString());
      setSection(data.section);

      const submission = (
        await axiosInstance.get(`assignment/submission/?assignment_id=${id}`)
      ).data;
      if (submission.status) {
        setSubmissionStatus('Reopened');
        setGradingStatus(submission.status);
        const updated = new Date(submission.updated_at).toDateString();
        setLastModified(updated);
        if (submission.grade) {
          setGrade(submission.grade);
        }
      }
    }
  }, [id]);

  useEffect(async () => {
    if (section) {
      const data = (
        await axiosInstance.get(`coursecontent/section/${section}/`)
      ).data;
      setCourseId(data.course.id);
    }
  }, [section]);

  useEffect(() => {
    const assignmentDueDate = new Date(duedate);
    const dateNow = Date.now();

    let timeDifference = assignmentDueDate.getTime() - dateNow;

    setIsLate(timeDifference <= 0);

    const days = Math.floor(timeDifference / (1000 * 3600 * 24));
    timeDifference = timeDifference - 1000 * 3600 * 24 * days;

    const hours = Math.floor(timeDifference / (1000 * 3600));
    timeDifference = timeDifference - 1000 * 3600 * hours;

    const minutes = Math.floor(timeDifference / (1000 * 60));
    timeDifference = timeDifference - 1000 * 60 * minutes;

    const seconds = Math.floor(timeDifference / 1000);

    setTimeRemaining(
      `${days} day(s), ${hours} hour(s), ${minutes} minute(s), ${seconds} second(s)`
    );
  }, [duedate]);

  useEffect(async () => {
    if (courseId) {
      try {
        const data = (await axiosInstance.get(`course/user/${courseId}/`)).data;
        if (data.role === 1) {
          setIsTeacher(true);
        }
      } catch {
        router.push(`/enrollment/${courseId}`);
      }
    }
  }, [courseId]);

  return (
    <Layout>
      <div className={detailassignment_styles.container}>
        <div className={detailassignment_styles.bodycontainer}>
          <Flex justifyContent="space-between">
            <Text fontSize="4xl" fontWeight="bold">
              {title}
            </Text>

            {isTeacher && (
              <Menu size="sm" border="none" placeholder="edit">
                <MenuButton>
                  Edit <b className={assignment_styles.caret}></b>
                </MenuButton>
                <MenuList>
                  <MenuItem>
                    <Link href={`/assignment/edit/${id}`}>Edit</Link>
                  </MenuItem>
                  <MenuItem>
                    <Link onClick={onOpen}>Delete</Link>
                  </MenuItem>
                  <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent>
                      <ModalHeader>Yakin ingin menghapus {title}?</ModalHeader>
                      <ModalFooter>
                        <Button
                          onClick={deleteAssignment}
                          mr="10px"
                          colorScheme="red"
                        >
                          Delete
                        </Button>
                        <Button colorScheme="blue" mr={3} onClick={onClose}>
                          Close
                        </Button>
                      </ModalFooter>
                    </ModalContent>
                  </Modal>
                </MenuList>
              </Menu>
            )}
          </Flex>
          <div className={detailassignment_styles.descriptioncontainer}>
            <Text>{description}</Text>
          </div>
          <div className={detailassignment_styles.submissioncontainer}>
            <Text fontSize="2xl" fontWeight="bold">
              Submission status
            </Text>
            <Table variant="striped" colorScheme="gray">
              <Tbody>
                <Tr>
                  <Td>Submission status</Td>
                  <Td>{isLate ? 'Submission closed' : submissionStatus}</Td>
                </Tr>
                <Tr>
                  <Td>Grading status</Td>
                  <Td>{gradingStatus}</Td>
                </Tr>
                <Tr>
                  <Td>Due date</Td>
                  <Td>{duedate}</Td>
                </Tr>
                <Tr>
                  <Td>Time remaining</Td>
                  <Td>{isLate ? 'Submission closed' : timeRemaining}</Td>
                </Tr>
                <Tr>
                  <Td>Last modified</Td>
                  <Td>{lastModified}</Td>
                </Tr>
                <Tr>
                  <Td>Grade</Td>
                  <Td>{grade}</Td>
                </Tr>
              </Tbody>
            </Table>

            {isTeacher && (
              <Center mt="20px" w="100%">
                <Link href={`/submission/list/${id}`}>
                  <Button background="#8ec63f" color="white">
                    View All Submission
                  </Button>
                </Link>
              </Center>
            )}
            {!isLate && grade == '-' && (
              <Center mt="20px" w="100%">
                <Link href={`/submission/add/${id}`}>
                  <Button background="#8ec63f" color="white">
                    Add Submission
                  </Button>
                </Link>
              </Center>
            )}
          </div>
        </div>
      </div>
    </Layout>
  );
}
