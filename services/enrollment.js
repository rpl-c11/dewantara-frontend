import axiosInstance from './axios';

export const getUserEnrollment = async () =>
  await axiosInstance.get(`enrollment/`);

export const isUserEnrolledToCourse = async (courseId) => {
  try {
    await axiosInstance.get(`course/user/${courseId}/`);
    return true;
  } catch {
    return false;
  }
};
