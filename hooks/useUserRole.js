import { useEffect, useState } from 'react';

const loadUserRole = () => {
  if (typeof window === 'undefined') return null;

  const userRole = localStorage.getItem('role');
  if (userRole) return userRole;
  return null;
};

const useUserRole = () => {
  const [userRole, setUserRole] = useState(loadUserRole);

  useEffect(() => {
    const handleChangeStorage = () => setUserRole(loadUserRole);

    if (typeof window !== 'undefined') {
      window.addEventListener('storage', handleChangeStorage);
      return () => window.removeEventListener('storage', handleChangeStorage);
    }
  }, []);

  /**
   * 0 -> User is Siswa
   * 1 -> User is Pengajar
   */
  return Number(userRole);
};

export default useUserRole;
