import Link from 'next/dist/client/link';
import { BsArrowRightCircle } from 'react-icons/bs';
import DeleteEdit from './DeleteEdit';
import { DivKanan, Judul } from './Styles';

const Kanan = (props) => {
  var isAuthor = false;
  if (typeof window !== 'undefined') {
    if (localStorage.getItem('username') == props.username) {
      isAuthor = true;
    }
  }
  const changeDate = (myDate) => {
    var time = new Date(myDate).toLocaleTimeString('en-UK', {
      weekday: 'long',
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: '2-digit',
      minute: '2-digit',
      hour12: true,
      timeZone: 'Asia/Jakarta',
    });
    time = time.replace('am', 'AM').replace('pm', 'PM');
    return time;
  };

  return (
    <DivKanan>
      <Link href={'/announcement/' + props.id}>
        <a>
          <Judul>{props.judul}</Judul>
        </a>
      </Link>
      <p style={{ fontSize: '0.8rem', marginBottom: '10px' }}>
        <BsArrowRightCircle style={{ display: 'inline' }} /> by {props.author} -
        {changeDate(props.date)}
      </p>
      <p
        style={{
          fontSize: '0.9rem',
          textAlign: 'justify',
          marginBottom: '15px',
        }}
      >
        {props.content}
      </p>
      {isAuthor && <DeleteEdit id={props.id} />}
    </DivKanan>
  );
};

export default Kanan;
