import { Button } from '@chakra-ui/button';
import { Center, Text } from '@chakra-ui/layout';
import { useToast } from '@chakra-ui/react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import Layout from '../../../components/Layout';
import addsubmission_styles from '../../../styles/assignments/AddSubmission.module.css';
import { default as baseURL } from '../../../services/config';

export default function AddSubmission() {
  const toast = useToast();
  const router = useRouter();
  const { id } = router.query;

  const [isUpload, setIsUpload] = useState(false);
  const [files, setFiles] = useState([]);
  const [file, setFile] = useState();

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      router.push('/login');
    }
  }, []);

  const onDrop = useCallback((acceptedFiles) => {
    setFiles(
      acceptedFiles.map((file) => {
        return (
          <li key={file.path}>
            {file.path} - {file.size} bytes
          </li>
        );
      })
    );
    setFile(acceptedFiles[0]);
    setIsUpload(true);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  async function submitAssignment() {
    const formData = new FormData();

    const userId = localStorage.getItem('userId');

    formData.append('file', file);
    formData.append('status', 'Not Graded');
    formData.append('assignment', id);
    formData.append('created_by', userId);
    formData.append('filename', file.name);

    const data = await axios.post(
      `${baseURL}assignment/submission/`,
      formData,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
          accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      }
    );
    setTimeout(() => {
      toast({
        title: 'File submitted',
        description: 'Submission successfully submitted',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }, 0);
    router.push(`/assignment/detail/${id}`);
  }

  return (
    <Layout>
      <div className={addsubmission_styles.container}>
        <div className={addsubmission_styles.bodycontainer}>
          <Text fontSize="4xl" fontWeight="bold">
            Tugas Individu 1
          </Text>
          <div className={addsubmission_styles.descriptioncontainer}>
            <Text>
              Tugas individu ini untuk melatih kemampuan individu anda
            </Text>
            <Text mt="10px" fontWeight="bold">
              File Submissions
            </Text>
            <Center>
              <section className={addsubmission_styles.dropzone}>
                <div {...getRootProps()}>
                  <input encType="multipart/form-data" {...getInputProps()} />
                  {!isUpload && (
                    <p>You can drag and drop files here to add them</p>
                  )}
                </div>
                <aside>
                  <ul>{files}</ul>
                </aside>
              </section>
            </Center>
            <Center mt="20px">
              <Button
                onClick={submitAssignment}
                colorScheme="green"
                disabled={!isUpload}
              >
                Submit
              </Button>
            </Center>
          </div>
        </div>
      </div>
    </Layout>
  );
}
