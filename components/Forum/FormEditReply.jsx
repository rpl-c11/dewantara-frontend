import {
  Button,
  Textarea,
  useToast,
  FormErrorMessage,
  Text,
} from '@chakra-ui/react';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';
import { useRouter } from 'next/router';
import Link from 'next/link';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { updateReply } from '../../services/forum';
import { ChevronLeftIcon } from '@chakra-ui/icons';

const FormEditReply = ({ judul, forumId, threadId, replyId, replyDetail }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm({ defaultValues: replyDetail });
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await updateReply(forumId, threadId, replyId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil mengedit reply!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.replace(`/forum/${forumId}/thread/${threadId}`);
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengedit reply',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
        <Link href={`/forum/${forumId}/thread/${threadId}`}>
          <Text
            cursor="pointer"
            fontSize="1rem"
            d="flex"
            alignItems="center"
            mt="1rem"
          >
            <ChevronLeftIcon mr="0.25rem" /> Kembali ke thread
          </Text>
        </Link>
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl id="reply" my={5} isRequired>
            <FormLabel>Konten Diskusi</FormLabel>
            <Textarea
              rows="10"
              {...register('reply', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.reply && errors.reply.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Reply
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormEditReply;
