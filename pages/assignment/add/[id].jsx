import { Button } from '@chakra-ui/button';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Flex, Square, Text } from '@chakra-ui/layout';
import { Select } from '@chakra-ui/select';
import { Textarea } from '@chakra-ui/textarea';
import { useToast } from '@chakra-ui/toast';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Layout from '../../../components/Layout';
import axiosInstance from '../../../services/axios';
import addassignment_styles from '../../../styles/assignments/AddAssignment.module.css';

export default function AddAssignment() {
  const router = useRouter();
  const { id } = router.query;
  const toast = useToast();
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const dates = [31, 28, 31, 30, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  const year = new Date().getFullYear();

  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [years, setYears] = useState(year);
  const [month, setMonth] = useState(new Date().getMonth());
  const [day, setDay] = useState(1);
  const [hour, setHour] = useState(0);
  const [minute, setMinute] = useState(0);

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      router.push('/login');
    }
  }, []);

  useEffect(async () => {
    if (id) {
      try {
        const data = (await axiosInstance.get(`course/user/${id}/`)).data;
        if (data.role === 0) {
          router.push(`/course/${id}`);
        }
      } catch {
        router.push(`/enrollment/${id}`);
      }
    }
  }, [id]);

  async function submitAssignment() {
    if (title.trim().length == 0) {
      toast({
        title: 'Data tidak boleh kosong',
        description: 'Harap isi kolom assignment name',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      return;
    }
    if (description.trim().length == 0) {
      toast({
        title: 'Data tidak boleh kosong',
        description: 'Harap isi kolom description',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      return;
    }
    const data = {
      day: parseInt(day),
      month: parseInt(month),
      year: parseInt(years),
      hour: parseInt(hour),
      minute: parseInt(minute),
      title: title.trim(),
      description: description.trim(),
      section: id,
    };
    const response = (await axiosInstance.post('assignment/', data)).data;

    toast({
      title: 'Success!',
      description: 'Berhasil menambahkan assignment!',
      status: 'success',
      duration: 5000,
      isClosable: true,
      position: 'top',
    });
    router.back();
  }

  return (
    <Layout>
      <div className={addassignment_styles.container}>
        <div className={addassignment_styles.bodycontainer}>
          <form>
            <Text mb="20px" fontSize="4xl">
              Add Assignment
            </Text>
            <FormControl id="name">
              <FormLabel>Assignment Name</FormLabel>
              <Input
                value={title}
                onChange={(event) => setTitle(event.target.value)}
                type="text"
              />
            </FormControl>
            <FormControl id="description">
              <FormLabel>Deskripsi</FormLabel>
              <Textarea
                value={description}
                onChange={(event) => setDescription(event.target.value)}
              />
            </FormControl>

            <FormLabel>Due Date</FormLabel>
            <Flex color="black">
              <Square mr="5px" w="10%" h="50px">
                <FormControl id="duedate_day">
                  <Select
                    value={day}
                    onChange={(event) => {
                      setDay(event.target.value);
                    }}
                  >
                    {Array.from(Array(dates[month]).keys()).map((i) => {
                      return (
                        <option key={i} value={i + 1}>
                          {i + 1}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Square>
              <Square mr="5px" w="20%" h="50px">
                <FormControl id="duedate_month">
                  <Select
                    value={monthNames[month]}
                    onChange={(event) =>
                      setMonth(monthNames.indexOf(event.target.value))
                    }
                  >
                    {monthNames.map((i) => {
                      return (
                        <option key={i} value={i}>
                          {i}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Square>
              <Square mr="5px" w="15%" h="50px">
                <FormControl id="duedate_year">
                  <Select
                    value={years}
                    onChange={(event) => setYears(event.target.value)}
                  >
                    {Array.from(Array(5).keys()).map((i) => {
                      return (
                        <option key={i} value={i + year}>
                          {i + year}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Square>
              <Square mr="5px" w="10%" h="50px">
                <FormControl id="duedate_day">
                  <Select
                    value={hour}
                    onChange={(event) => setHour(event.target.value)}
                  >
                    {Array.from(Array(24).keys()).map((i) => {
                      return (
                        <option key={i} value={i}>
                          {i}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Square>
              <Square mr="5px" w="10%" h="50px">
                <FormControl id="duedate_day">
                  <Select
                    value={minute}
                    onChange={(event) => setMinute(event.target.value)}
                  >
                    {Array.from(Array(12).keys()).map((i) => {
                      return (
                        <option key={i} value={5 * i}>
                          {5 * i}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Square>
            </Flex>

            <Button onClick={submitAssignment} colorScheme="green">
              Save
            </Button>
          </form>
        </div>
      </div>
    </Layout>
  );
}
