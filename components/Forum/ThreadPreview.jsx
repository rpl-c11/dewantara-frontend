import Link from 'next/link';
import { Td } from '@chakra-ui/react';

import * as S from './Styles';
import { convertUTCToString } from '../../utils/date';

const ThreadPreview = ({ data, forumId }) => {
  const { id, title, created_by, created_at, replies } = data;

  const lastPost = !replies.length
    ? `${created_by} - ${convertUTCToString(created_at)}`
    : `${replies[replies.length - 1].created_by} - ${convertUTCToString(
        replies[replies.length - 1].created_at
      )}`;

  return (
    <S.ThreadPreview>
      <Td>
        <Link href={`/forum/${forumId}/thread/${id}`}>{title}</Link>
      </Td>
      <Td>{created_by}</Td>
      <Td>{replies.length}</Td>
      <Td>{lastPost}</Td>
    </S.ThreadPreview>
  );
};

export default ThreadPreview;
