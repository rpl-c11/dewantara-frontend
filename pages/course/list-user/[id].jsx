// import { useRouter } from 'next/dist/client/router';
// import Layout from '../../../components/Layout';
// import UserItem from '../../../components/Enrollment/UserItem';
// import course_styles from '../../../styles/Course.module.css';
// import Link from 'next/dist/client/link';
// import { Button } from '@chakra-ui/button';
// import { useEffect, useState } from "react";
// import axiosInstance from '../../../services/axios';

// export default function ListUser() {
//   const router = useRouter();
//   const { id } = router.query;
//   const [listUser, setListUser] = useState([])
//   const [isTeacher, setIsTeacher] = useState(false);

//   useEffect(() => {
//     const fetchData = async () => {
//       if (id) {
//         const res = await axiosInstance.get(`/course/${id}/list`)
//         setListUser(res.data)
//       }
//     }
//     fetchData()
//   }, [id])

//   useEffect(() => {
//     const role = localStorage.getItem('role');
//     if (role === '1') {
//       setIsTeacher(true);
//     }
//   }, [])

//   return (
//     <Layout>
//       <div className={course_styles.bodycontainer}>
//         <div className={course_styles.container}>
//           <table>
//             <thead>
//               <tr>
//                 <th>Nama</th>
//                 <th>Role</th>
//                 <th></th>
//                 <th></th>
//               </tr>
//             </thead>
//             <tbody>
//               {listUser.map((e, i) => (
//                 <UserItem isTeacher={isTeacher} key={i} name={e.name} role={e.role} />
//               ))}
//             </tbody>
//           </table>
//         </div>
//       </div>
//       <Link href="/course/Course%20Title">
//         <Button variant="outline" size="md" width="80px" borderColor="#000">
//           Kembali
//         </Button>
//       </Link>
//     </Layout>
//   );
// }
import { useRouter } from 'next/dist/client/router';
import UserItem from '../../../components/Enrollment/UserItem';
import Layout from '../../../components/Layout';
import course_styles from '../../../styles/Course.module.css';
import Link from 'next/dist/client/link';
import { Button } from '@chakra-ui/button';
import submissionlist_styles from '../../../styles/assignments/SubmissionList.module.css';
import { Table, Thead, Tr, Th, Tbody, Td, Text, Flex } from '@chakra-ui/react';
import Image from 'next/image';
import axiosInstance from '../../../services/axios';
import { useEffect, useState } from 'react';

export default function ListUser() {
  const router = useRouter();
  const { id } = router.query;
  const [listUser, setListUser] = useState([]);
  const [isTeacher, setIsTeacher] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      if (id) {
        const res = await axiosInstance.get(`/course/${id}/list`);
        setListUser(res.data);
      }
    };
    fetchData();
  }, [id]);

    useEffect(() => {
      const role = localStorage.getItem('role');
      if (role === '1') {
        setIsTeacher(true);
      }

    
    }, [])

    async function deleteEnroll(enrollment_id){
      console.log(id)
      await axiosInstance.delete(`enrollment/${enrollment_id}/`,{data: {
      
      pk: id,
      }});
      router.reload();
    } 
    async function ubahEnroll(enrollment_id){
      console.log(id)
      await axiosInstance.put(`enrollment/${enrollment_id}/`, {
      
      pk: id,
      });
      router.reload();
    } 
    return (
        <Layout>
            <div className={course_styles.bodycontainer}>
                <div className={course_styles.container}>
                <Text fontSize='4xl' fontWeight='bold'>Peserta Kelas</Text>
                {listUser.length}
                    <Table variant='striped'>
                        <Thead>
                            <Tr>
                                <Th>Nama</Th>
                                <Th>Role</Th>
                                <Th></Th>
                                <Th></Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                        {listUser.map((e, i) => (
                          <UserItem isTeacher={isTeacher} key={i} name={e.user.name} role={e.role} id= {e.id} onDelete={deleteEnroll} onUbah={ubahEnroll}/>
                        ))}  
                            
                        </Tbody>
                    </Table>
                </div>
            </div>
            <Link href="/course/Course%20Title">
                <Button
                  variant="outline"
                  size="md"
                  width="80px"
                  borderColor="#000"
                >
                  Kembali
                </Button>
            </Link>
        </Layout>
    )
}
