import {
  Button,
  Textarea,
  Input,
  useToast,
  FormErrorMessage,
  Heading,
} from '@chakra-ui/react';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';
import { useRouter } from 'next/router';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { updateThread } from '../../services/forum';

const FormEditThread = ({ judul, forumId, threadId, threadDetail }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm({ defaultValues: threadDetail });
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await updateThread(forumId, threadId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil mengedit thread!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.replace(`/forum/${forumId}/thread/${threadId}`);
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengedit thread',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
        <Heading fontSize="1.25rem" mt="1rem">
          {threadDetail?.title}
        </Heading>
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl id="judul" my={5} isRequired>
            <FormLabel>Judul</FormLabel>
            <Input
              type="text"
              {...register('title', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.title && errors.title.message}
            </FormErrorMessage>
          </FormControl>
          <FormControl id="isi" my={5} isRequired>
            <FormLabel>Konten Diskusi</FormLabel>
            <Textarea
              rows="10"
              {...register('body', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.body && errors.body.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Thread
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormEditThread;
