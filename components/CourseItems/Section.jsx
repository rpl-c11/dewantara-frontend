import { useEffect, useState } from 'react';
import axiosInstance from '../../services/axios';
import section_styles from '../../styles/courseitems/Section.module.css';
import AddActivityItem from './AddActivityItem';
import AssignmentItem from './AssignmentItem';
import ForumItem from './ForumItem';

export default function Section({ name, id, isEdit }) {
  const [contents, setContent] = useState([]);

  useEffect(async () => {
    if (id) {
      const data = (await axiosInstance.get(`coursecontent/list/${id}/`)).data;
      setContent(data);
    }
  }, [id]);

  return (
    <div className={section_styles.sectioncontainer}>
      <div>
        <h3 className={section_styles.sectionname}>{name}</h3>
      </div>
      <div>
        {contents.map((content) => {
          if (content.type === 'assignment') {
            return (
              <AssignmentItem
                isEdit={isEdit}
                name={content.name}
                id={content.id}
              />
            );
          } else {
            return (
              <ForumItem isEdit={isEdit} name={content.name} id={content.id} />
            );
          }
        })}
      </div>
      {isEdit && <AddActivityItem id={id} />}
    </div>
  );
}
