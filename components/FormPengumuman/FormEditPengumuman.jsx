import { Button } from '@chakra-ui/button';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Flex } from '@chakra-ui/layout';
import { Textarea } from '@chakra-ui/textarea';
import { useRouter } from 'next/dist/client/router';
import { useState } from 'react';
import Swal from 'sweetalert2';
import axiosInstance from '../../services/axios';
import LoginBox, { OuterBox } from './Styles';

const FormEditPengumuman = ({ announcement }) => {
  const router = useRouter();
  const [announcementData, setAnnouncementData] = useState(announcement);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setAnnouncementData({
      ...announcementData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    await axiosInstance
      .put(`announcement/${announcement.id}/`, announcementData)
      .then((res) => {
        Swal.fire({
          title: 'Success!',
          text: 'Berhasil Mengedit Pengumuman!',
          icon: 'success',
          confirmButtonText: 'OK',
        }).then(router.push('/'));
        router.push('/');
      })
      .catch((err) => console.log(err));
  };

  return (
    <OuterBox>
      <h2
        style={{ fontSize: '1.5rem', fontWeight: 'bold', textAlign: 'center' }}
      >
        Edit Pengumuman
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit}>
          <FormControl id="judul" my={5} isRequired>
            <FormLabel>Judul</FormLabel>
            <Input
              name="title"
              type="text"
              onChange={handleChange}
              value={announcementData.title}
            />
          </FormControl>
          <FormControl id="isi" my={5} isRequired>
            <FormLabel>Isi Pengumuman</FormLabel>
            <Textarea
              name="content"
              rows="10"
              onChange={handleChange}
              value={announcementData.content}
            />
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
            >
              Simpan Pengumuman
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormEditPengumuman;
