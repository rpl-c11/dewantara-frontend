import { Button } from '@chakra-ui/button';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Select } from '@chakra-ui/react';
import { useSession } from 'next-auth/client';
import Link from 'next/dist/client/link';
import { useRouter } from 'next/dist/client/router';
import { useState } from 'react';
import { axioss } from '../../services/axios';
import LoginBox, { OuterBox } from './Styles';
import Swal from 'sweetalert2';

const FormGoogleSignup = () => {
  const [session, loading] = useSession();
  const router = useRouter();
  const [signupData, setSignupData] = useState({
    role: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignupData({
      ...signupData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log(signupData);

    await axioss
      .post(
        `auth/register/google/`,
        {
          role: signupData.role,
        },
        {
          headers: {
            Authorization: 'Bearer ' + session.accessToken,
            'Content-Type': 'application/json',
          },
        }
      )
      .then((res) => {
        router.push('/login');
      })
      .catch((err) => {
        Swal.fire({
          title: 'Error!',
          text: err.response.data.detail,
          icon: 'error',
          confirmButtonText: 'OK',
        });
      });
  };

  return (
    <OuterBox>
      <h2
        style={{ fontSize: '1.5rem', fontWeight: 'bold', textAlign: 'center' }}
      >
        Sign Up to Continue
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit}>
          <FormControl id="name" my={5} isRequired>
            <FormLabel>Name</FormLabel>
            <Input
              name="name"
              type="text"
              value={session && session.name}
              disabled
            />
          </FormControl>
          <FormControl id="email" my={5} isRequired>
            <FormLabel>Email address</FormLabel>
            <Input
              name="email"
              type="email"
              value={session && session.email}
              disabled
            />
          </FormControl>
          <FormControl id="username" my={5} isRequired>
            <FormLabel>Username</FormLabel>
            <Input
              name="username"
              type="text"
              value={session && session.username}
              disabled
            />
          </FormControl>
          <FormControl id="role" my={5} isRequired>
            <FormLabel>Role</FormLabel>
            <Select
              name="role"
              onChange={handleChange}
              placeholder="Select Role"
            >
              <option value="0">Siswa</option>
              <option value="1">Pengajar</option>
            </Select>
          </FormControl>
          <Button my={1} width="100%" type="submit">
            Signup
          </Button>
        </form>
        <p>
          Already have an account?{' '}
          <Link href="/login/">
            <a style={{ color: 'blue' }}>Login Here</a>
          </Link>
        </p>
      </LoginBox>
    </OuterBox>
  );
};

export default FormGoogleSignup;
