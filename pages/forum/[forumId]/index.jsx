import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import {
  Button,
  Heading,
  HStack,
  Text,
  useDisclosure,
  useToast,
  Box,
} from '@chakra-ui/react';
import {
  Modal,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ModalOverlay,
} from '@chakra-ui/modal';

import {
  deleteForumDetail,
  getForumDetail,
  getForumThreads,
} from '../../../services/forum';
import { convertUTCToString } from '../../../utils/date';
import useUserRole from '../../../hooks/useUserRole';
import Layout from '../../../components/Layout';
import * as S from '../../../components/Forum/Styles';
import ThreadList from '../../../components/Forum/ThreadList';
import { overrideButtonVariantToRed } from '../../../styles/overrides';
import { isUserEnrolledToCourse } from '../../../services/enrollment';
import { redirectToHome } from '../../../utils/route';
import { ChevronLeftIcon } from '@chakra-ui/icons';

export default function Forum() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const toast = useToast();

  const [forumDetail, setForumDetail] = useState(null);
  const [forumThreads, setForumThreads] = useState(null);
  const [isUserEnrolled, setUserEnrolled] = useState(null);

  const isUserPengajar = useUserRole() === 1;

  useEffect(() => {
    if (router.isReady) {
      const { forumId } = router.query;
      fetchForumDetail(forumId);
      fetchForumThreads(forumId);
    }
  }, [router.isReady]);

  useEffect(() => {
    if (forumDetail) fetchUserEnrollment();
  }, [forumDetail]);

  useEffect(() => {
    console.log(isUserEnrolled);
  }, [isUserEnrolled]);

  const fetchForumDetail = async (forumId) => {
    try {
      const forumDetailResponse = await getForumDetail(forumId);
      const forumDetailData = forumDetailResponse.data;
      setForumDetail(forumDetailData);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data forum',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  const fetchForumThreads = async (forumId) => {
    try {
      const forumThreadsResponse = await getForumThreads(forumId);
      const forumThreadsData = forumThreadsResponse.data;
      setForumThreads(forumThreadsData);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data threads',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  const fetchUserEnrollment = async () => {
    try {
      const isUserEnrolledResponse = await isUserEnrolledToCourse(
        forumDetail?.course_id
      );
      setUserEnrolled(isUserEnrolledResponse);
      if (!isUserEnrolledResponse) redirectToHome(router);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan melihat role',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  const deleteForum = async () => {
    try {
      await deleteForumDetail(forumDetail?.id);
      toast({
        title: 'Success',
        description: 'Forum berhasil di hapus!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.replace(`/course/${forumDetail?.course_id}`);
      }, 1500);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menghapus forum',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <>
      {forumDetail && isUserEnrolled ? (
        <div>
          <Head>
            <title>{forumDetail?.name}</title>
          </Head>

          <Layout>
            <S.Container>
              <Link href={`/course/${forumDetail?.course_id}`}>
                <Heading
                  cursor="pointer"
                  d="flex"
                  alignItems="center"
                  fontSize="1.15rem"
                >
                  <ChevronLeftIcon color="black" />
                  Back to Course
                </Heading>
              </Link>
              <Heading>{forumDetail?.name}</Heading>
              <Text fontWeight="normal" fontSize="1rem" mb="1.25rem">
                by {forumDetail?.created_by} -{' '}
                {convertUTCToString(forumDetail?.created_at)}
              </Text>

              <HStack d="block" mx="auto" spacing={4}>
                <Link href={`/forum/${forumDetail?.id}/thread/add`}>
                  <Button disabled={!forumDetail?.is_active}>
                    Tambahkan Thread
                  </Button>
                </Link>
                {isUserPengajar && (
                  <>
                    <Link href={`/forum/${forumDetail?.id}/edit`}>
                      <Button
                        variant="outline"
                        disabled={!forumDetail?.is_active}
                      >
                        Edit Forum
                      </Button>
                    </Link>
                    <Button
                      disabled={!forumDetail?.is_active}
                      {...overrideButtonVariantToRed}
                      onClick={onOpen}
                    >
                      Hapus Forum
                    </Button>

                    <Modal isOpen={isOpen} onClose={onClose}>
                      <ModalOverlay />
                      <ModalContent>
                        <ModalHeader>Yakin ingin menghapus Forum?</ModalHeader>
                        <ModalBody>
                          Seluruh thread yang ada di forum ini akan dihapus
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            {...overrideButtonVariantToRed}
                            onClick={deleteForum}
                            mr="10px"
                          >
                            Delete
                          </Button>
                          <Button mr={3} onClick={onClose}>
                            Close
                          </Button>
                        </ModalFooter>
                      </ModalContent>
                    </Modal>
                  </>
                )}
              </HStack>

              <>
                {forumThreads && forumDetail && (
                  <ThreadList
                    forumId={forumDetail?.id}
                    forumThreads={forumThreads}
                  />
                )}
              </>
            </S.Container>
          </Layout>
        </div>
      ) : (
        <></>
      )}
    </>
  );
}
