import { Text } from '@chakra-ui/layout';
import { Box, Stack } from '@chakra-ui/react';

export const Footer = () => (
  <Box
    as="footer"
    role="contentinfo"
    mx="auto"
    maxW="7xl"
    py="12"
    px={{ base: '4', md: '8' }}
  >
    <Stack>
      <Text alignSelf={{ base: 'center', sm: 'start' }} fontSize="sm">
        &copy; {new Date().getFullYear()} Dewantara. All rights reserved.
      </Text>
    </Stack>
  </Box>
);
