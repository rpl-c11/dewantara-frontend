import Link from 'next/dist/client/link';
import DeleteDialog from './Delete';

export default function DeleteEdit(props) {
  return (
    <div style={{ textAlign: 'right', fontSize: '0.8rem' }}>
      <Link href={'/announcement/edit/' + props.id}>
        <a style={{ margin: 'auto 10px' }}>Edit</a>
      </Link>
      <DeleteDialog {...props} />
    </div>
  );
}
