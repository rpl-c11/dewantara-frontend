import { Button } from '@chakra-ui/button';
import { useDisclosure } from '@chakra-ui/hooks';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from '@chakra-ui/modal';
import { useState } from 'react';
import addactivity_styles from '../../styles/courseitems/AddActivity.module.css';
import Image from 'next/image';
import { Input } from '@chakra-ui/input';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { useRouter } from 'next/router';
import axiosInstance from '../../services/axios';

export default function AddSection({ courseId }) {
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [sectionName, setSectionName] = useState('');

  async function submit() {
    await axiosInstance.post('coursecontent/', {
      courseId: courseId,
      name: sectionName,
    });
    onClose();
    router.reload();
    setSectionName('');
  }

  return (
    <>
      <div className={addactivity_styles.left}>
        <div className={addactivity_styles.addactivitycontainer}>
          <Button background="gray" onClick={onOpen}>
            <Image
              width={20}
              height={20}
              src="https://scele.cs.ui.ac.id/theme/image.php/lambda/core/1636195207/t/add"
              alt="add-activity"
              className={addactivity_styles.img}
            />
            <span>Add new section</span>
          </Button>
        </div>
      </div>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Tambah Section</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Section name</FormLabel>
              <Input
                value={sectionName}
                onChange={(event) => setSectionName(event.target.value)}
                type="text"
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button
              variant={sectionName.trim().length > 0 ? 'solid' : 'ghost'}
              colorScheme={sectionName.trim().length > 0 && 'green'}
              disabled={sectionName.trim().length == 0}
              onClick={submit}
            >
              Add
            </Button>
            <Button colorScheme="blue" ml={3} onClick={onClose}>
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
