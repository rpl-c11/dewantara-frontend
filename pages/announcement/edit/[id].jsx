import { useRouter } from 'next/dist/client/router';
import Head from 'next/head';
import { useEffect } from 'react';
import FormEditPengumuman from '../../../components/FormPengumuman/FormEditPengumuman';
import Layout from '../../../components/Layout';
import { axioss } from '../../../services/axios';
import styles from '/styles/Home.module.css';

export const getServerSideProps = async (context) => {
  const id = context.params.id;
  var data = null;
  await axioss
    .get(`announcement/${id}/`)
    .then((res) => {
      data = {
        props: { announcement: res.data },
      };
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return data;
};

export default function Edit({ announcement }) {
  var isDosen = false;
  const router = useRouter();

  if (typeof window !== 'undefined') {
    if (localStorage.getItem('role') == 1) {
      isDosen = true;
    }
  }

  useEffect(() => {
    if (!isDosen) {
      router.push('/');
    }
  }, []);

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Edit Pengumuman</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormEditPengumuman announcement={announcement} />
      </div>
    </Layout>
  );
}
