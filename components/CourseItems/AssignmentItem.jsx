import { Flex, Link } from '@chakra-ui/layout';
import { Menu, MenuButton, MenuItem, MenuList } from '@chakra-ui/menu';
import { useDisclosure } from '@chakra-ui/hooks';
import assignment_styles from '../../styles/courseitems/Assignment.module.css';
import {
  Modal,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/modal';
import { Button } from '@chakra-ui/button';
import Image from 'next/image';
import { useRouter } from 'next/router';
import axiosInstance from '../../services/axios';

export default function AssignmentItem({ name, id, isEdit }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const router = useRouter();

  async function deleteAssignment() {
    await axiosInstance.delete(`/assignment/${id}/`);
    router.reload();
  }

  return (
    <Flex justifyContent="space-between">
      <Link href={`/assignment/detail/${id}`}>
        <div className={assignment_styles.assignmentcontainer}>
          <Image
            width={20}
            height={20}
            src="https://scele.cs.ui.ac.id/theme/image.php/lambda/assign/1636195207/icon"
            alt="assignment-icon"
            className={assignment_styles.img}
          />
          <span>{name}</span>
        </div>
      </Link>
      <div>
        {isEdit && (
          <Menu size="sm" border="none" placeholder="edit">
            <MenuButton>
              Edit <b className={assignment_styles.caret}></b>
            </MenuButton>
            <MenuList>
              <MenuItem>
                <Link href={`/assignment/edit/${id}`}>Edit</Link>
              </MenuItem>
              <MenuItem>
                <Link onClick={onOpen}>Delete</Link>
              </MenuItem>
              <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                  <ModalHeader>Yakin ingin menghapus {name}?</ModalHeader>
                  <ModalFooter>
                    <Button
                      onClick={deleteAssignment}
                      mr="10px"
                      colorScheme="red"
                    >
                      Delete
                    </Button>
                    <Button colorScheme="blue" mr={3} onClick={onClose}>
                      Close
                    </Button>
                  </ModalFooter>
                </ModalContent>
              </Modal>
            </MenuList>
          </Menu>
        )}
      </div>
    </Flex>
  );
}
