import { useRouter } from 'next/dist/client/router';
import CourseHeader from '../../components/CourseItems/CourseHeader';
import Section from '../../components/CourseItems/Section';
import Layout from '../../components/Layout';
import course_styles from '../../styles/Course.module.css';
import Link from 'next/dist/client/link';
import { Flex } from '@chakra-ui/layout';
import { Button } from '@chakra-ui/button';
import AddSection from '../../components/CourseItems/AddSection';
import { useEffect, useState } from 'react';
import axiosInstance from '../../services/axios';

export default function Course() {
  const router = useRouter();
  const { id } = router.query;
  const [sections, setSections] = useState([]);

  const [isEditMode, setIsEditMode] = useState(false);
  const [isTeacher, setIsTeacher] = useState(false);
  const [course, setCourse] = useState({});

  useEffect(async () => {
    if (id) {
      const data = (await axiosInstance.get(`coursecontent/?courseId=${id}`))
        .data;
      setSections(data);
    }
  }, [id]);

  useEffect(async () => {
    if (id) {
      try {
        const data = (await axiosInstance.get(`course/user/${id}/`)).data;
        if (data.role === 1) {
          setIsTeacher(true);
        }
      } catch {
        router.push(`/enrollment/${id}`);
      }
    }
  }, [id]);

  useEffect(async () => {
    if (id) {
      const data = (await axiosInstance.get(`course/${id}/`)).data;
      setCourse(data);
    }
  }, [id]);

  async function deleteEnroll(){
    await axiosInstance.delete('enrollment/', {

    pk: id,
    });
    router.reload();
  } 
  return (
    <Layout>
      <div className={course_styles.bodycontainer}>
        <div className={course_styles.container}>
          {isTeacher && (
            <Button
              onClick={() => setIsEditMode(!isEditMode)}
              background={isEditMode ? 'red' : 'green'}
            >
              {isEditMode ? 'Turn off' : 'Turn on'} edit mode
            </Button>
          )}
          <CourseHeader name={course?.title} />
          <Flex justifyContent="space-between">
            <Link href={`/course/list-user/${id}`}>Peserta Kelas</Link>
            <Link href="/enrollment/1">Unenroll Kelas</Link>
          </Flex>

          {sections.map((section) => (
            <Section isEdit={isEditMode} name={section.name} id={section.id} />
          ))}

          {isEditMode && <AddSection courseId={id} />}
        </div>
      </div>
    </Layout>
  );
}
