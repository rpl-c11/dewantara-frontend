import { Box, Flex } from '@chakra-ui/react';
import { Button } from '@chakra-ui/button';
import Link from 'next/dist/client/link';
import Layout from '../../components/Layout';
import CourseCard from '../../components/CourseItems/CourseCard';
import course_styles from '../../styles/Course.module.css';
import UserItem from '../../components/Enrollment/UserItem';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import axiosInstance from '../../services/axios';

export default function EnrollCourse() {
  const router = useRouter();
  const { id } = router.query;
  
  useEffect(async () => {
    if (id) {
      try {
        const data = (await axiosInstance.get(`course/user/${id}/`)).data;
        
        router.push(`/course/${id}`)
      } catch {
        
        
      }
    }
  }, [id])
  
  async function postEnroll(){
    await axiosInstance.post('enrollment/', {

    pk: id,
    });
    router.reload();
  } 
  return (
    <Layout>
      <div className={course_styles.bodycontainer}>
        <div className={course_styles.container}>
          <Flex
            px={10}
            direction="column"
            height={100}
            justifyContent="space-between"
          >
            {<CourseCard />}
          </Flex>
          
          
            <Button
              bg="black"
              variant="ghost"
              color="white"
              _hover={{ bg: 'black' }}
              _focus={{ bg: 'black' }}
              onClick={postEnroll}
            > Enroll
            </Button>
          
        </div>
      </div>
    </Layout>
  );
}
