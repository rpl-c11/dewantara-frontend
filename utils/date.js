export const convertUTCToString = (utc) => {
  const utcDate = new Date(utc);
  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };
  const timeOptions = {
    hour: '2-digit',
    minute: '2-digit',
    hour12: true,
  };

  return `
    ${utcDate.toLocaleDateString('en-US', dateOptions)},
    ${utcDate.toLocaleTimeString([], timeOptions)}
  `;
};

export const getNMinutesFromDate = (utc, minutes) =>
  new Date(new Date(utc).getTime() + minutes * 60000);
