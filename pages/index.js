import { Flex } from '@chakra-ui/react';
import Head from 'next/head';
import AddAnnouncementButton from '../components/Announcement/AddAnnouncement';
import Announcement from '../components/Announcement/Announcement';
import Layout from '../components/Layout';
import announcement_styles from '../styles/Announcement.module.css';
import { axioss } from '../services/axios';
import { useState } from 'react';
import { useEffect } from 'react';

export const getServerSideProps = async () => {
  var data = null;
  await axioss
    .get(`announcement/`)
    .then((res) => {
      data = {
        props: { announcements: res.data },
      };
    })
    .catch((err) => {
      console.log(err);
      return null;
    });

  return data;
};

export default function Home({ announcements }) {
  // const [state, setState] = useState({
  //   announcements: [],
  // });

  // useEffect(async () => {}, []);
  const [isPengajar, setPengajar] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (localStorage.getItem('role') == 1) {
        setPengajar(true);
      }
    }
  }, []);

  return (
    <Layout>
      <div className="body-container">
        <Head>
          <title>Dewantara</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <Flex justify="center" direction="column" align="center">
          <h1
            style={{
              textAlign: 'center',
              margin: '24px auto',
              fontSize: '1.5rem',
              fontWeight: 'bold',
            }}
          >
            Pengumuman
          </h1>

          <div className={announcement_styles.announcementcontainer}>
            {isPengajar && <AddAnnouncementButton />}
            {announcements.map((announcement) => (
              <Announcement
                key={announcement.id}
                id={announcement.id}
                judul={announcement.title}
                date={announcement.created_at}
                author={announcement.created_by.name}
                username={announcement.created_by.username}
                content={announcement.content}
              ></Announcement>
            ))}
          </div>
        </Flex>
      </div>
    </Layout>
  );
}
