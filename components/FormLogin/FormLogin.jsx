import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Button } from '@chakra-ui/button';
import LoginBox, { OuterBox } from './Styles';
import Link from 'next/dist/client/link';
import { Center, HStack } from '@chakra-ui/layout';
import { FcGoogle } from 'react-icons/fc';
import { useRouter } from 'next/dist/client/router';
import { useState } from 'react';
import axiosInstance from '../../services/axios';
import { axioss } from '../../services/axios';
import { signIn } from 'next-auth/client';
import Swal from 'sweetalert2';

const FormLogin = () => {
  const router = useRouter();
  const [loginData, setLoginData] = useState({
    email: '',
    password: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setLoginData({
      ...loginData,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axioss
      .post(`auth/login/`, loginData)
      .then((res) => {
        // console.log(res)
        if (res.data.is_verified == 0) {
          Swal.fire({
            title: 'Error!',
            text: 'Maaf akun mu belum diverifikasi',
            icon: 'error',
            confirmButtonText: 'OK',
          });
          router.push('/');
        } else {
          localStorage.setItem('name', res.data.name);
          localStorage.setItem('email', res.data.email);
          localStorage.setItem('username', res.data.username);
          localStorage.setItem('role', res.data.role);
          localStorage.setItem('is_verified', res.data.is_verified);
          localStorage.setItem('accessToken', res.data.tokens.access);
          localStorage.setItem('refreshToken', res.data.tokens.refresh);
          localStorage.setItem('userId', res.data.id);
          axiosInstance.defaults.headers['Authorization'] =
            'Bearer ' + localStorage.getItem('accessToken');
          router.push('/');
        }
      })
      .catch((err) => {
        Swal.fire({
          title: 'Error!',
          text: err.response.data.detail,
          icon: 'error',
          confirmButtonText: 'OK',
        });
      });
  };

  var isLogin = false;

  if (typeof window !== 'undefined') {
    if (localStorage.getItem('role')) {
      isLogin = true;
    }
    if (isLogin) {
      router.push('/');
    }
  }

  return (
    <OuterBox>
      <h2
        style={{ fontSize: '1.5rem', fontWeight: 'bold', marginTop: '-3rem' }}
      >
        Welcome Back
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit}>
          <FormControl id="email" my={5} isRequired>
            <FormLabel>Email address</FormLabel>
            <Input type="email" name="email" onChange={handleChange} />
          </FormControl>
          <FormControl my={5} id="password" isRequired>
            <FormLabel>Password</FormLabel>
            <Input type="password" name="password" onChange={handleChange} />
          </FormControl>
          <Button my={1} width="100%" type="submit">
            Login
          </Button>
        </form>
        <HStack my={3}>
          <Button
            leftIcon={<FcGoogle />}
            w={'full'}
            variant={'outline'}
            onClick={() =>
              signIn('google', {
                callbackUrl: `${window.location.origin}/google-signin/`,
              })
            }
          >
            <Center>Continue with Google</Center>
          </Button>
        </HStack>
        <p>
          Don&apos;t have account?{' '}
          <Link href="/signup/">
            <a style={{ color: 'blue' }}>Register Here</a>
          </Link>
        </p>
      </LoginBox>
    </OuterBox>
  );
};

export default FormLogin;
