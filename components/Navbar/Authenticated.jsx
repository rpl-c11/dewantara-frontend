import { Text } from '@chakra-ui/layout';
import Link from 'next/dist/client/link';
import { ButtonAuth } from './Styles';

export default function Authenticated(props) {
  return (
    <>
      <li className="d-flex align-items-center nav-item ms-auto my-lg-0 my-1 mx-2">
        <Text fontSize="1.35em" fontWeight="700">
          {' '}
          {props.name}{' '}
        </Text>
      </li>
      <li className="nav-item ms-auto my-lg-0 my-1">
        <Link href="/logout">
          <ButtonAuth size="md" width="80px" color="#fff" background="red">
            Log Out
          </ButtonAuth>
        </Link>
      </li>
    </>
  );
}
