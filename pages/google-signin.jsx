import { useSession } from 'next-auth/client';
import { useRouter } from 'next/dist/client/router';
import axiosInstance from '../services/axios';
import Swal from 'sweetalert2';

export default function GoogleSignin() {
  const router = useRouter();
  const [session, loading] = useSession();

  const handleSession = () => {
    if (session.role == -1) {
      router.push('/google-signup/');
    }
    if (session.is_verified == 0) {
      Swal.fire({
        title: 'Error!',
        text: 'Maaf akun mu belum diverifikasi',
        icon: 'error',
        confirmButtonText: 'OK',
      });
      router.push('/');
    } else {
      localStorage.setItem('name', session.name);
      localStorage.setItem('email', session.email);
      localStorage.setItem('username', session.username);
      localStorage.setItem('role', session.role);
      localStorage.setItem('is_verified', session.is_verified);
      localStorage.setItem('accessToken', session.accessToken);
      localStorage.setItem('refreshToken', session.refreshToken);
      axiosInstance.defaults.headers['Authorization'] =
        'Bearer ' + localStorage.getItem('accessToken');
      router.push('/');
    }
  };

  return <>{session && handleSession()}</>;
}
