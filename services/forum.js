import axiosInstance from './axios';

export const createForum = async (sectionId, requestBody) =>
  await axiosInstance.post(`forum/create/${sectionId}/`, requestBody);

export const getForumDetail = async (forumId) =>
  await axiosInstance.get(`forum/${forumId}/`);

export const updateForumDetail = async (forumId, requestBody) =>
  await axiosInstance.put(`forum/${forumId}/`, requestBody);

export const deleteForumDetail = async (forumId) =>
  await axiosInstance.delete(`forum/${forumId}/`);

export const getForumThreads = async (forumId) =>
  await axiosInstance.get(`forum/${forumId}/thread/`);

export const createThread = async (forumId, requestBody) =>
  await axiosInstance.post(`forum/${forumId}/thread/create/`, requestBody);

export const getThread = async (forumId, threadId) =>
  await axiosInstance.get(`forum/${forumId}/thread/${threadId}/`);

export const updateThread = async (forumId, threadId, requestBody) =>
  await axiosInstance.put(`forum/${forumId}/thread/${threadId}/`, requestBody);

export const deleteThread = async (forumId, threadId) =>
  await axiosInstance.delete(`forum/${forumId}/thread/${threadId}/`);

export const createReply = async (forumId, threadId, requestBody) =>
  await axiosInstance.post(
    `forum/${forumId}/thread/${threadId}/reply/create/`,
    requestBody
  );

export const getReply = async (forumId, threadId, replyId) =>
  await axiosInstance.get(
    `forum/${forumId}/thread/${threadId}/reply/${replyId}/`
  );

export const updateReply = async (forumId, threadId, replyId, requestBody) =>
  await axiosInstance.put(
    `forum/${forumId}/thread/${threadId}/reply/${replyId}/`,
    requestBody
  );

export const deleteReply = async (forumId, threadId, replyId) =>
  await axiosInstance.delete(
    `forum/${forumId}/thread/${threadId}/reply/${replyId}/`
  );
