import { Thead, Tbody, Tr, Th } from '@chakra-ui/react';

import * as S from './Styles';
import ThreadPreview from './ThreadPreview';

const ThreadList = ({ forumThreads, forumId }) => (
  <S.ThreadList>
    <Thead>
      <Tr>
        <Th>Title</Th>
        <Th>Started by</Th>
        <Th isNumeric>Replies</Th>
        <Th>Last post</Th>
      </Tr>
    </Thead>

    <Tbody>
      {forumThreads.map((thread, id) => (
        <ThreadPreview forumId={forumId} data={thread} key={id} />
      ))}
    </Tbody>
  </S.ThreadList>
);

export default ThreadList;
