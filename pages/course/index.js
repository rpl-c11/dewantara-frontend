import { Box, Flex, Button, Text } from '@chakra-ui/react';
import Layout from '../../components/Layout';
import CourseCard from '../../components/CourseItems/CourseCard';
import axiosInstance from "../../services/axios";
import { useEffect, useState } from 'react';
import Link from 'next/dist/client/link';

export default function ListCourse() {
  const [courses, setCourses] = useState([])
  
  let isDosen = false;

  if (typeof window !== 'undefined') {
    isDosen = localStorage.getItem('role') == 1
  }

  useEffect(() => {
    axiosInstance
      .get(`course/`)
      .then((res) => {
        setCourses(res?.data)
      })
    }, [])

  return (
    <Layout>
      <Flex>
        <Flex
          width="75%"
          px={10}
          direction="column"
          height={courses?.length * 100}
          justifyContent="space-between"
        >
          {courses?.map((course, index) => {
            return <CourseCard
                    key={course?.id}
                    id={course?.id}
                    title={course?.title}
                    description={course?.description}
                    isDosen={isDosen}
                  />;
          })}
        </Flex>
        {isDosen ? 
          <Flex width="25%" justifyContent="center">
            <Box>
              <Text fontSize="3xl" fontWeight="semi-bold">
                Add New Course
              </Text>
              <Link href="/course/add">
                <Button fontSize="2xl" width="100%">
                  +
                </Button>
              </Link>
            </Box>
          </Flex>
        :
          <> </>
        }
      </Flex>
    </Layout>
  );
}
