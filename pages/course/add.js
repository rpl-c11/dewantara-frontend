import {
  Box,
  Flex,
  Button,
  Text,
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
  NumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { useRouter } from 'next/dist/client/router';
import Swal from 'sweetalert2';
import Layout from '../../components/Layout';
import axiosInstance from '../../services/axios';
import { useState, useEffect } from 'react';

export default function AddCourse() {
  const router = useRouter()

  let isDosen = false;

  if (typeof window !== 'undefined') {
    isDosen = localStorage.getItem('role') == 1
  }

  useEffect(() => {
    if (!isDosen) {
      router.push('/');
    }
  }, []);

  const [courseData, setCourseData] = useState({
    title: '',
    description: '',
  })

  const handleChange = (e) => {
    const { name, value } = e.target
    setCourseData({
      ...courseData,
      [name]: value,
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    await axiosInstance
      .post('course/', courseData)
      .then((res) => {
        Swal.fire({
          title: 'Success!',
          icon: 'success'
        }).then(router.push('/course'))
      })
      .catch((err) => console.log(err))
  }


  return (
    <Layout>
      <Flex px="20%" py={5} direction="column" width="100%">
        <form onSubmit={e => handleSubmit(e)}>
          <FormControl>
            <FormLabel>Course Name</FormLabel>
            <Input name='title' type='text' onChange={e => handleChange(e)} />
          </FormControl>
          <FormControl>
            <FormLabel>Brief Description</FormLabel>
            <Input name='description' type='text' onChange={e => handleChange(e)} />
          </FormControl>
          <Button 
            colorScheme="blue"
            type='submit'
          >
              Submit
          </Button>
        </form>
      </Flex>
    </Layout>
  );
}
