module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['scele.cs.ui.ac.id'],
  },
  env: {
    API_BASE_URL: process.env.API_BASE_URL,
  },
};
