const Heading = {
  baseStyle: {
    margin: '24px auto',
    fontWeight: 'bold',
    fontSize: '1.5rem',
  },
};

export default Heading;
