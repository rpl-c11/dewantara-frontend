import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalBody,
  useDisclosure,
  Button,
  Link as ChakraLink,
  useToast,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import Link from 'next/link';

import { convertUTCToString, getNMinutesFromDate } from '../../utils/date';
import { overrideButtonVariantToRed } from '../../styles/overrides';
import { deleteReply } from '../../services/forum';
import * as S from '../../components/Forum/Styles';
import useUserId from '../../hooks/useUserId';

const ThreadReply = ({
  id: replyId,
  forumId,
  threadId,
  created_by_id,
  created_by,
  created_at,
  updated_at,
  reply,
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const isCreatedByUser = useUserId() === created_by_id;
  const isEditable = new Date() <= getNMinutesFromDate(created_at, 30);
  const isEdited =
    new Date(updated_at).getTime() - new Date(created_at).getTime() > 5000;

  const router = useRouter();
  const toast = useToast();

  const onDeleteReply = async () => {
    try {
      await deleteReply(forumId, threadId, replyId);
      toast({
        title: 'Success',
        description: 'Reply berhasil di hapus!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.reload();
      }, 1500);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menghapus reply',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Yakin ingin menghapus Reply?</ModalHeader>
          <ModalBody>Reply yang dihapus tidak dapat dikembalikan</ModalBody>
          <ModalFooter>
            <Button
              {...overrideButtonVariantToRed}
              onClick={onDeleteReply}
              mr="10px"
            >
              Delete
            </Button>
            <Button mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      <S.ThreadReply>
        <div className="reply__image">
          <Image
            alt={created_by}
            src="/profpic.png"
            objectFit="contain"
            width={50}
            height={50}
            layout="responsive"
          />
        </div>

        <div className="reply__content">
          <h6 className="user">{`by ${created_by} - ${convertUTCToString(
            created_at
          )}`}</h6>
          <p className="content">{reply}</p>

          {isCreatedByUser && (
            <div className="reply__content__footer">
              {isEdited && (
                <h6 className="edited-at">
                  Edited at {convertUTCToString(updated_at)}
                </h6>
              )}
              <div className="navigation">
                {isEditable && (
                  <Link
                    href={`/forum/${forumId}/thread/${threadId}/reply/${replyId}/edit`}
                  >
                    <h6 className="edit">Edit</h6>
                  </Link>
                )}
                <ChakraLink onClick={onOpen}>
                  <h6 className="delete">Delete</h6>
                </ChakraLink>
              </div>
            </div>
          )}
        </div>
      </S.ThreadReply>
    </>
  );
};

export default ThreadReply;
