import { useEffect, useState } from 'react';
import { useToast } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { getReply } from '../../../../../../../services/forum';
import FormEditReply from '../../../../../../../components/Forum/FormEditReply';
import Layout from '../../../../../../../components/Layout';
import styles from '/styles/Home.module.css';

export default function EditThread() {
  const router = useRouter();
  const toast = useToast();
  const [forumId, setForumId] = useState(null);
  const [threadId, setThreadId] = useState(null);
  const [replyId, setReplyId] = useState(null);
  const [replyDetail, setReplyDetail] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const {
        forumId: routerForumId,
        threadId: routerThreadId,
        replyId: routerReplyId,
      } = router.query;
      setForumId(routerForumId);
      setThreadId(routerThreadId);
      setReplyId(routerReplyId);
      fetchReplyDetail(routerForumId, routerThreadId, routerReplyId);
    }
  }, [router.isReady]);

  const fetchReplyDetail = async (forumId, threadId, replyId) => {
    try {
      const replyDetailResponse = await getReply(forumId, threadId, replyId);
      const { reply } = await replyDetailResponse.data;
      setReplyDetail({ reply });
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data reply',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Edit Reply</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {replyDetail && (
          <FormEditReply
            forumId={forumId}
            threadId={threadId}
            replyId={replyId}
            replyDetail={replyDetail}
            judul="Edit Reply"
          />
        )}
      </div>
    </Layout>
  );
}
