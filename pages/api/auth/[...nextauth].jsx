import NextAuth from 'next-auth';
import Providers from 'next-auth/providers';
import { axioss } from '../../../services/axios';

export default NextAuth({
  providers: [
    Providers.Google({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      state: false,
      session: {
        jwt: false,
        maxAge: 10 * 60,
      },
    }),
  ],
  callbacks: {
    async signIn(user, account, profile) {
      const { access_token, id_token } = account;
      try {
        const response = await axioss
          .post(`social_auth/google/`, {
            access_token,
            id_token,
          })
          .then((res) => {
            const { name, email, username, is_verified, role } = res.data;
            const { access, refresh } = res.data.tokens;
            user.is_verified = is_verified;
            user.role = role;
            user.username = username;
            user.accessToken = access;
            user.refreshToken = refresh;
            return user;
          });
      } catch (error) {
        console.log(error);
      }

      return true;
    },

    async jwt(token, user, account, profile, isNewUser) {
      if (user) {
        console.log(user);
        token.username = user.username;
        token.accessToken = user.accessToken;
        token.is_verified = user.is_verified;
        token.refreshToken = user.refreshToken;
        token.role = user.role;
      }
      return token;
    },

    async session(session, user) {
      session.name = user.name;
      session.email = user.email;
      session.username = user.username;
      session.accessToken = user.accessToken;
      session.is_verified = user.is_verified;
      session.refreshToken = user.refreshToken;
      session.role = user.role;
      return session;
    },
  },
});
