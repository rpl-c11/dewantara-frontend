import { useRouter } from 'next/router';

import useUserRole from '../hooks/useUserRole';

const requirePengajar = (Component) => {
  return (props) => {
    const userRole = useUserRole();

    if (typeof window !== 'undefined') {
      const router = useRouter();

      if (userRole !== 1) {
        router.replace('/');
        return null;
      }

      return <Component {...props} />;
    }

    return null;
  };
};

export default requirePengajar;
