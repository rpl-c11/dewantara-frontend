import { Box, Flex, Spacer, Text, Button } from '@chakra-ui/react';
import Link from 'next/dist/client/link';

export default function CourseCard(props) {
  const {id, title, description, isDosen} = props

  return (
    <Box w="100%" bg="gray.200" p={2}>
      <Text fontSize="2xl" fontWeight="medium">
        {title}
      </Text>
      <Flex>
        {isDosen ? 
          <div>
            <Link href={`/course/update/${id}`}>
              <Button>
                Update
              </Button>
            </Link>
            <Link href={`/course/delete/${id}`}>
              <Button>
                Delete
              </Button>
            </Link>
          </div>
        :
          <> </>
        }
        <Spacer />
        <Text>{description}</Text>
      </Flex>
    </Box>
  );
}
