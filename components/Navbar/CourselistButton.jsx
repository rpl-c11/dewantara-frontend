import { useEffect, useState } from 'react';
import { Menu, MenuButton, MenuList, MenuItem, Button } from '@chakra-ui/react';
import CourseButton from './CourseButton';
import axiosInstance from '../../services/axios';
import Link from 'next/dist/client/link';

export default function CourselistButton() {
  const [userCourses, setUserCourses] = useState([]);

  useEffect(async () => {
    try {
      const data = (await axiosInstance.get('course/user/')).data;
      setUserCourses(data);
    } catch (error) {}
  }, []);

  return (
    <Menu>
      <Link href='/course'>
        <MenuButton as={Button} background="gray" mr="5px">
          My courses
        </MenuButton>
      </Link>
      <MenuList>
        {userCourses.map((course) => {
          console.log(course);
          return <CourseButton id={course.id}>{course.title}</CourseButton>;
        })}
      </MenuList>
    </Menu>
  );
}
