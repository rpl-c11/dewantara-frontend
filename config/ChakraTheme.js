import { extendTheme } from '@chakra-ui/react';

import Button from '../components/UIElements/Button';
import Heading from '../components/UIElements/Heading';

const theme = extendTheme({
  components: {
    Button,
    Heading,
  },
});

export default theme;
