import courseheader_styles from '../../styles/courseitems/CourseHeader.module.css';

export default function CourseHeader({ name }) {
  return (
    <div className={courseheader_styles.sectioncontainer}>
      <div>
        <h3 className={courseheader_styles.sectionname}>{name}</h3>
      </div>
    </div>
  );
}
