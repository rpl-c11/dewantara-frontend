import Head from 'next/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';

import FormAddForum from '../../../components/Forum/FormAddForum';
import Layout from '../../../components/Layout';
import requirePengajar from '../../../hoc/requirePengajar';
import styles from '/styles/Home.module.css';

const AddForum = () => {
  const router = useRouter();
  const [sectionId, setSectionId] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { sectionId } = router.query;
      setSectionId(sectionId);
    }
  }, [router.isReady]);

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Tambahkan Forum</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormAddForum sectionId={sectionId} judul="Tambahkan Forum" />
      </div>
    </Layout>
  );
};

export default requirePengajar(AddForum);
