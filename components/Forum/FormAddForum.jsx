import {
  FormControl,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/form-control';
import { Button, Input, useToast } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { createForum } from '../../services/forum';
import { useRouter } from 'next/router';

const FormAddForum = ({ sectionId, judul }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await createForum(sectionId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil membuat forum!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.back();
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menambahkan forum',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={!!errors.name} id="judul" isRequired my={5}>
            <FormLabel>Judul</FormLabel>
            <Input
              type="text"
              {...register('name', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.name && errors.name.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Forum
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormAddForum;
