module.exports = {
  '**/*.(js|jsx|json)': (filenames) => [
    `npm run prettier --write ${filenames.join(' ')}`,
  ],
};
