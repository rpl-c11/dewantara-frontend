import { Button } from '@chakra-ui/button';
import { useDisclosure } from '@chakra-ui/hooks';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from '@chakra-ui/modal';
import { Radio, RadioGroup } from '@chakra-ui/radio';
import Link from 'next/dist/client/link';
import { useState } from 'react';
import addactivity_styles from '../../styles/courseitems/AddActivity.module.css';
import Image from 'next/image';

export default function AddActivityItem({ id }) {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [value, setValue] = useState();
  const [isClick, setIsClick] = useState(false);

  function getLink() {
    return value == 'Assignment' ? `/assignment/add/${id}` : `/forum/add/${id}`;
  }

  return (
    <>
      <div className={addactivity_styles.left}>
        <div className={addactivity_styles.addactivitycontainer}>
          <Button background="gray" onClick={onOpen}>
            <Image
              width={20}
              height={20}
              src="https://scele.cs.ui.ac.id/theme/image.php/lambda/core/1636195207/t/add"
              alt="add-activity"
              className={addactivity_styles.img}
            />
            <span>Add an activity or resource</span>
          </Button>
        </div>
      </div>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Tambah Aktivitas</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <RadioGroup
              onChange={(val) => {
                setValue(val);
                setIsClick(true);
              }}
              value={value}
            >
              <div>
                <Radio value="Assignment">
                  <div className={addactivity_styles.addactivitycontainer}>
                    <Image
                      width={20}
                      height={20}
                      src="https://scele.cs.ui.ac.id/theme/image.php/lambda/assign/1636195207/icon"
                      alt="assignment"
                      className={addactivity_styles.img}
                    />
                    <span>Assignment</span>
                  </div>
                </Radio>
              </div>
              <div>
                <Radio value="Forum">
                  <div className={addactivity_styles.addactivitycontainer}>
                    <Image
                      width={20}
                      height={20}
                      src="https://scele.cs.ui.ac.id/theme/image.php/lambda/forum/1636195207/icon"
                      alt="assignment"
                      className={addactivity_styles.img}
                    />
                    <span>Forum</span>
                  </div>
                </Radio>
              </div>
            </RadioGroup>
          </ModalBody>

          <ModalFooter>
            <Link href={getLink()}>
              <Button
                variant={isClick ? 'solid' : 'ghost'}
                colorScheme={isClick && 'green'}
                disabled={!isClick}
              >
                Add
              </Button>
            </Link>
            <Button colorScheme="blue" ml={3} onClick={onClose}>
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
