import styled from 'styled-components';
import { Button } from '@chakra-ui/button';

const Nav = styled.nav`
  @media (min-width: 991px) {
    padding: 8px 15vw;
  }
`;

const ButtonAuth = styled(Button)`
  margin: auto 5px;
  border-radius: 2px !important;
  @media (max-width: 991px) {
    width: 90vw !important;
    margin: auto 2vw;
  }
`;

export { Nav, ButtonAuth };
