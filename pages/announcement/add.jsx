import { useRouter } from 'next/dist/client/router';
import Head from 'next/head';
import { useEffect } from 'react';
import FormPengumuman from '../../components/FormPengumuman/FormPengumuman';
import Layout from '../../components/Layout';
import styles from '/styles/Home.module.css';

export default function Add() {
  var isDosen = false;
  const router = useRouter();

  if (typeof window !== 'undefined') {
    if (localStorage.getItem('role') == 1) {
      isDosen = true;
    }
  }

  useEffect(() => {
    if (!isDosen) {
      router.push('/');
    }
  }, []);

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Tambah Pengumuman</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormPengumuman judul="Tambahkan Pengumuman" />
      </div>
    </Layout>
  );
}
