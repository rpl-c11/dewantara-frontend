import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalBody,
  useDisclosure,
  Button,
  Link as ChakraLink,
  useToast,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import Image from 'next/image';
import Link from 'next/link';

import { overrideButtonVariantToRed } from '../../styles/overrides';
import { convertUTCToString } from '../../utils/date';
import { deleteThread } from '../../services/forum';
import * as S from '../../components/Forum/Styles';
import useUserId from '../../hooks/useUserId';

const ThreadMainPost = ({
  id: threadId,
  forum_id: forumId,
  created_by_id,
  created_by,
  created_at,
  updated_at,
  title,
  body,
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const toast = useToast();

  const isCreatedByUser = useUserId() === created_by_id;
  const isEdited =
    new Date(updated_at).getTime() - new Date(created_at).getTime() > 5000;

  const onDeleteThread = async () => {
    try {
      await deleteThread(forumId, threadId);
      toast({
        title: 'Success',
        description: 'Thread berhasil di hapus!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.replace(`/forum/${forumId}`);
      }, 1500);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menghapus thread',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Yakin ingin menghapus Thread?</ModalHeader>
          <ModalBody>
            Thread beserta seluruh reply yang ada di thread ini akan dihapus
          </ModalBody>
          <ModalFooter>
            <Button
              {...overrideButtonVariantToRed}
              onClick={onDeleteThread}
              mr="10px"
            >
              Delete
            </Button>
            <Button mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      <S.ThreadReply>
        <div className="reply__image">
          <Image
            alt={created_by}
            src="/profpic.png"
            objectFit="contain"
            width={50}
            height={50}
            layout="responsive"
          />
        </div>

        <div className="reply__content">
          <h6 className="title">{title}</h6>
          <h6 className="user">{`by ${created_by} - ${convertUTCToString(
            created_at
          )}`}</h6>
          <p className="content">{body}</p>

          {isCreatedByUser && (
            <div className="reply__content__footer">
              {isEdited && (
                <h6 className="edited-at">
                  Edited at {convertUTCToString(updated_at)}
                </h6>
              )}
              <div className="navigation">
                <Link href={`/forum/${forumId}/thread/${threadId}/reply/add`}>
                  <h6 className="add-reply">Reply</h6>
                </Link>
                <Link href={`/forum/${forumId}/thread/${threadId}/edit`}>
                  <h6 className="edit">Edit</h6>
                </Link>
                <ChakraLink onClick={onOpen}>
                  <h6 className="delete">Delete</h6>
                </ChakraLink>
              </div>
            </div>
          )}
        </div>
      </S.ThreadReply>
    </>
  );
};

export default ThreadMainPost;
