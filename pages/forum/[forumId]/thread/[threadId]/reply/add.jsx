import Head from 'next/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';

import FormAddReply from '../../../../../../components/Forum/FormAddReply';
import Layout from '../../../../../../components/Layout';
import styles from '/styles/Home.module.css';

export default function CreateReply() {
  const router = useRouter();
  const [forumId, setForumId] = useState(null);
  const [threadId, setThreadId] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { forumId: routerForumId, threadId: routerThreadId } = router.query;
      setForumId(routerForumId);
      setThreadId(routerThreadId);
    }
  }, [router.isReady]);

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Tambahkan Forum</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormAddReply
          judul="Tambahkan Reply"
          forumId={forumId}
          threadId={threadId}
        />
      </div>
    </Layout>
  );
}
