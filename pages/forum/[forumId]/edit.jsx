import Head from 'next/head';
import { useRouter } from 'next/router';
import { useToast } from '@chakra-ui/react';
import { useState, useEffect } from 'react';

import { getForumDetail } from '../../../services/forum';
import FormEditForum from '../../../components/Forum/FormEditForum';
import Layout from '../../../components/Layout';
import styles from '/styles/Home.module.css';

export default function EditForum() {
  const router = useRouter();
  const toast = useToast();
  const [forumId, setForumId] = useState(null);
  const [forumDetail, setForumDetail] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { forumId: routerForumId } = router.query;
      setForumId(routerForumId);
      fetchForumDetail(routerForumId);
    }
  }, [router.isReady]);

  const fetchForumDetail = async (forumId) => {
    try {
      const forumDetailData = await getForumDetail(forumId);
      const { name } = await forumDetailData.data;
      setForumDetail({ name });
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data forum',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Edit Forum | {forumDetail?.name}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {forumDetail && (
          <FormEditForum
            forumId={forumId}
            forumDetail={forumDetail}
            judul="Edit Forum"
          />
        )}
      </div>
    </Layout>
  );
}
