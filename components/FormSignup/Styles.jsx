import { Box } from '@chakra-ui/layout';
import styled from 'styled-components';

const LoginBox = styled(Box)`
  width: 40vw;
  margin-top: 1.5rem;
  @media (max-width: 1024px) {
    width: 80vw;
  }
`;

const OuterBox = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 80vh;
  padding: 5vw 10vw;
  @media (min-width: 1024px) {
    margin: auto 15vw;
  }
`;
export default LoginBox;
export { OuterBox };
