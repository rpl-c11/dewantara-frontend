import styled from 'styled-components';
import { Box, Table, Tr } from '@chakra-ui/react';

export const Container = styled(Box).attrs({
  textAlign: 'center',
  width: '60vw',
  minH: '75vh',
  my: '2.5rem',
  mx: 'auto',
})``;

export const ThreadList = styled(Table)`
  margin-top: 2.5rem;
`;

export const ThreadPreview = styled(Tr)`
  &:nth-child(odd) {
    background-color: #e2e8f0;
  }

  &:nth-child(odd) {
    background-color: #f5f5f5;
  }
`;

export const ThreadDetail = styled(Box).attrs({
  p: '1.5rem 1rem',
  textAlign: 'left',
})``;

export const ThreadReply = styled(Box)`
  background-color: #e2e8f0;
  border-width: 1px 1px 1px 5px;
  border-color: #718096;
  border-radius: 3px;

  display: flex;
  margin: 1rem 0;
  padding: 1.25rem;

  & div.reply__image {
    width: 10%;
    margin-right: 1rem;

    & img {
      border-radius: 50%;
      overflow: hidden;
      flex-shrink: 0;
    }
  }

  & div.reply__content {
    width: 90%;

    & h6.title {
      font-weight: bold;
      font-size: 1.15rem;
      margin-bottom: 0.5rem;
    }

    & h6.user {
      margin-bottom: 0.75rem;
      font-size: 0.875rem;
    }

    & p.content {
      line-height: 1.5;
    }

    & div.reply__content__footer {
      display: flex;
      justify-content: space-between;

      margin-top: 1.5rem;
      font-size: 0.875rem;

      & div.navigation {
        display: flex;
        margin-left: auto;

        & h6.edit,
        & h6.delete {
          margin-left: 0.75rem;
        }

        & h6.add-reply,
        & h6.edit,
        & h6.delete {
          cursor: pointer;

          &:hover {
            text-decoration: underline;
          }
        }
      }
    }
  }
`;

export const ThreadReplies = styled(Box)`
  margin-left: 3rem;
`;
