import { useEffect } from 'react';
import axiosInstance from '../services/axios';
import { useRouter } from 'next/dist/client/router';

export default function Logout() {
  const router = useRouter();

  useEffect(async () => {
    await axiosInstance
      .post('auth/logout/', {
        refresh: localStorage.getItem('refreshToken'),
      })
      .catch((err) => console.log(err));

    localStorage.clear();
    axiosInstance.defaults.headers['Authorization'] = null;
    router.push('/');
  });

  return null;
}
