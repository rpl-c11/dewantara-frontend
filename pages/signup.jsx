import Head from 'next/head';
import FormSignup from '../components/FormSignup/FormSignup';
import Layout from '../components/Layout';
import styles from '../styles/Home.module.css';

export default function Signup() {
  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Sign Up</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormSignup />
      </div>
    </Layout>
  );
}
