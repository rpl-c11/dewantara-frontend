import {
    Flex,
    Button,
    FormControl,
    FormLabel,
    Input,
  } from '@chakra-ui/react';
  import Layout from '../../../components/Layout';
  import { useRouter } from 'next/router';
  import { useEffect, useState } from 'react';
  import axiosInstance from '../../../services/axios';
  import Swal from 'sweetalert2';
  
  export default function AddCourse() {
    const router = useRouter()
    const { id } = router.query

    let isDosen = false;

    if (typeof window !== 'undefined') {
      isDosen = localStorage.getItem('role') == 1
    }
  
    useEffect(() => {
      if (!isDosen) {
        router.push('/');
      }
    }, []);
  
    const [courseData, setCourseData] = useState({
      title: '',
      description: '',
    })
  
    useEffect(() => {
      if (id===undefined || id=='' || !isDosen){
        return
      } 
      axiosInstance
      .get(`course/${id}/`)
      .then((res) => {
        setCourseData(res?.data)
        Swal.fire({
          title: `Go back if you do not want to delete this course`,
          icon: 'info'
        })
      })
      .catch((err) => {
        console.log(err)
        Swal.fire({
          title: 'Illegal Action',
          icon: 'error'
        })
        .then(router.push('/course'))
      })
      
    }, [id])
  
    const handleSubmit = async (e) => {
      e.preventDefault()
      await axiosInstance
        .delete(`course/${id}/`)
        .then((res) => {
          Swal.fire({
            title: 'Deleted!',
            icon: 'info'
          }).then(router.push('/course'))
        })
        .catch((err) => console.log(err))
    }
  
    
  
    return (
      <Layout>
        <Flex px="20%" py={5} direction="column" width="100%">
          <form onSubmit={e => handleSubmit(e)}>
            <FormControl>
              <FormLabel>Course Name</FormLabel>
              <Input readOnly name='title' value={courseData?.title} />
            </FormControl>
            <FormControl>
              <FormLabel>Brief Description</FormLabel>
              <Input readOnly name='description'  value={courseData?.description} />
            </FormControl>
            <Button type='submit' colorScheme='blue'>Delete</Button>
          </form>
        </Flex>
      </Layout>
    );
  }
  