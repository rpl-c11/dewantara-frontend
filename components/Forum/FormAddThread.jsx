import {
  Button,
  Textarea,
  Input,
  useToast,
  FormErrorMessage,
} from '@chakra-ui/react';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';
import { useRouter } from 'next/router';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { createThread } from '../../services/forum';

const FormAddThread = ({ judul, forumId }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await createThread(forumId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil membuat thread!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.replace(`/forum/${forumId}`);
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menambahkan thread',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl id="judul" my={5} isRequired>
            <FormLabel>Judul</FormLabel>
            <Input
              type="text"
              {...register('title', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.title && errors.title.message}
            </FormErrorMessage>
          </FormControl>
          <FormControl id="isi" my={5} isRequired>
            <FormLabel>Konten Diskusi</FormLabel>
            <Textarea
              rows="10"
              {...register('body', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.body && errors.body.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Thread
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormAddThread;
