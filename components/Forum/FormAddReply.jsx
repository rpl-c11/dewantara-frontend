import {
  FormControl,
  FormLabel,
  FormErrorMessage,
} from '@chakra-ui/form-control';
import { Button, Input, useToast } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { Flex } from '@chakra-ui/layout';

import LoginBox, { OuterBox } from '../FormPengumuman/Styles';
import { createReply } from '../../services/forum';
import { useRouter } from 'next/router';

const FormAddReply = ({ forumId, threadId, judul }) => {
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();
  const toast = useToast();
  const router = useRouter();

  const onSubmit = async (data) => {
    try {
      await createReply(forumId, threadId, data);
      toast({
        title: 'Success!',
        description: 'Berhasil menambahkan reply!',
        status: 'success',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
      setTimeout(() => {
        router.back();
      }, 1500);
    } catch (_) {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika menambahkan reply',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <OuterBox>
      <h2
        style={{
          fontSize: '1.5rem',
          fontWeight: 'bold',
          textAlign: 'center',
        }}
      >
        {judul}
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={!!errors.reply} id="reply" isRequired my={5}>
            <FormLabel>Reply</FormLabel>
            <Input
              type="text"
              {...register('reply', {
                required: 'This is required',
              })}
            />
            <FormErrorMessage>
              {errors.reply && errors.reply.message}
            </FormErrorMessage>
          </FormControl>
          <Flex>
            <Button
              my={1}
              ml="auto"
              type="submit"
              variant="solid"
              textAlign="left"
              isLoading={isSubmitting}
            >
              Simpan Reply
            </Button>
          </Flex>
        </form>
      </LoginBox>
    </OuterBox>
  );
};

export default FormAddReply;
