import {
  Table,
  Thead,
  Tr,
  Th,
  Tbody,
  Td,
  Text,
  Flex,
  Input,
} from '@chakra-ui/react';
import Layout from '../../../components/Layout';
import submissionlist_styles from '../../../styles/assignments/SubmissionList.module.css';
import Image from 'next/image';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import axiosInstance from '../../../services/axios';

export default function SubmissionList() {
  const router = useRouter();
  const { id } = router.query;
  const [submissions, setSubmissions] = useState([]);

  useEffect(() => {
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) {
      router.push('/login');
    }
  }, []);

  useEffect(async () => {
    if (id) {
      try {
        const data = (await axiosInstance.get(`course/user/${id}/`)).data;
        if (data.role === 0) {
          router.push(`/course/${id}`);
        }
      } catch {
        router.push(`/enrollment/${id}`);
      }
    }
  }, [id]);

  useEffect(async () => {
    if (id) {
      const data = (await axiosInstance.get(`assignment/submissions/${id}/`))
        .data;
      const subs = data.map((item) => {
        return {
          name: item.created_by.name,
          status: item.status,
          submission: item.file,
          last_modified: new Date(item.updated_at).toDateString(),
          final_grade: item.grade || '-',
          id: item.id,
          filename: item.filename,
        };
      });

      setSubmissions(subs);
    }
  }, [id]);

  async function handleChangeGrade(event, id) {
    let nilai = event.target.value;
    if (!nilai) {
      nilai = 0;
    }
    nilai = parseInt(nilai);
    if (nilai > 100) {
      nilai = 100;
    }
    const new_submission = submissions.map((submission) => {
      return {
        ...submission,
        final_grade: submission.id == id ? nilai : submission.final_grade,
      };
    });
    setSubmissions(new_submission);

    await axiosInstance.patch(`assignment/submissions/${id}/`, {
      grade: nilai,
    });
  }

  return (
    <Layout>
      <div className={submissionlist_styles.container}>
        <div className={submissionlist_styles.bodycontainer}>
          <Text fontSize="4xl" fontWeight="bold">
            Tugas Individu 1
          </Text>
          <Table variant="striped">
            <Thead>
              <Tr>
                <Th>Name</Th>
                <Th>Status</Th>
                <Th>Submission</Th>
                <Th>Last Modified</Th>
                <Th>Final Grade</Th>
              </Tr>
            </Thead>
            <Tbody>
              {submissions.map((submission) => {
                return (
                  <Tr>
                    <Td>{submission.name}</Td>
                    <Td>{submission.status}</Td>
                    <Td>
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={submission.submission}
                      >
                        <Flex>
                          <div>
                            <Image
                              width={20}
                              height={20}
                              src="https://scele.cs.ui.ac.id/theme/image.php/lambda/core/1636195207/f/pdf-24"
                              class="iconlarge activityicon"
                              alt="pdf"
                              role="presentation"
                              className={submissionlist_styles.img}
                            />
                          </div>
                          <div>
                            <Text>{submission.filename}</Text>
                          </div>
                        </Flex>
                      </a>
                    </Td>
                    <Td>{submission.last_modified}</Td>
                    <Td>
                      <Input
                        onChange={(event) =>
                          handleChangeGrade(event, submission.id)
                        }
                        type="text"
                        value={submission.final_grade}
                        background="white"
                      ></Input>
                    </Td>
                  </Tr>
                );
              })}
            </Tbody>
          </Table>
        </div>
      </div>
    </Layout>
  );
}
