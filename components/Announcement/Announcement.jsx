import { Flex } from '@chakra-ui/layout';
import Kanan from './Kanan';
import Kiri from './Kiri';

const Announcement = (props) => {
  return (
    <Flex
      bg="gray.200"
      w="95%"
      borderWidth="1px"
      borderRadius="sm"
      borderLeftWidth="5px"
      borderColor="gray.500"
      py={5}
      px={6}
      gridColumnGap="5"
      align="flex-start"
    >
      <Kiri />
      <Kanan {...props} />
    </Flex>
  );
};

export default Announcement;
