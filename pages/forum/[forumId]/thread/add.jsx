import Head from 'next/head';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';

import FormAddThread from '../../../../components/Forum/FormAddThread';
import Layout from '../../../../components/Layout';
import styles from '/styles/Home.module.css';

export default function CreateThread() {
  const router = useRouter();
  const [forumId, setForumId] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { forumId } = router.query;
      setForumId(forumId);
    }
  }, [router.isReady]);

  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Tambahkan Forum</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormAddThread judul="Tambahkan Thread" forumId={forumId} />
      </div>
    </Layout>
  );
}
