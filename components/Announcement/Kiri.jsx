import Image from 'next/image';
import { DivKiri } from './Styles';

const Kiri = () => {
  return (
    <DivKiri>
      <Image
        alt="profpic"
        src="/profpic.png"
        objectFit="contain"
        width={50}
        height={50}
        layout="responsive"
      ></Image>
    </DivKiri>
  );
};

export default Kiri;
