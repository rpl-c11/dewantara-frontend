import Link from 'next/dist/client/link';
import { useEffect, useState } from 'react';
import Authenticated from './Authenticated';
import CourselistButton from './CourselistButton';
import NotAuthenticated from './NotAuthenticated';
import { Nav } from './Styles';

const Navbar = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [name, setName] = useState('');

  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (localStorage.getItem('accessToken') != null) {
        setIsAuthenticated(true);
        setName(localStorage.getItem('name'));
      }
    }
  }, []);

  return (
    <Nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <Link href="/">
          <a
            className="navbar-brand"
            style={{ fontSize: '1.5rem', fontWeight: '700' }}
          >
            Dewantara
          </a>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse justify-content-end "
          id="navbarNav"
        >
          <ul className="navbar-nav">
            {isAuthenticated ? (
              <>
                <Authenticated name={name} />
                <li className="nav-item ms-auto my-lg-0 my-1">
                  <CourselistButton />
                </li>
              </>
            ) : (
              <NotAuthenticated />
            )}
          </ul>
        </div>
      </div>
    </Nav>
  );
};

export default Navbar;
