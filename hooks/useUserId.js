import { useEffect, useState } from 'react';

const loadUserId = () => {
  if (typeof window === 'undefined') return null;

  const userId = localStorage.getItem('userId');
  if (userId) return userId;
  return null;
};

const useUserId = () => {
  const [userId, setUserId] = useState(loadUserId);

  useEffect(() => {
    const handleChangeStorage = () => setUserId(loadUserId);

    if (typeof window !== 'undefined') {
      window.addEventListener('storage', handleChangeStorage);
      return () => window.removeEventListener('storage', handleChangeStorage);
    }
  }, []);

  return Number(userId);
};

export default useUserId;
