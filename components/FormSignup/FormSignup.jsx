import { Button } from '@chakra-ui/button';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { Center, HStack } from '@chakra-ui/layout';
import { Select } from '@chakra-ui/react';
import { signIn } from 'next-auth/client';
import Link from 'next/dist/client/link';
import { useRouter } from 'next/dist/client/router';
import { useState } from 'react';
import { FcGoogle } from 'react-icons/fc';
import Swal from 'sweetalert2';
import { axioss } from '../../services/axios';
import LoginBox, { OuterBox } from './Styles';

const FormSignup = () => {
  const router = useRouter();
  const [signupData, setSignupData] = useState({
    name: '',
    email: '',
    username: '',
    password: '',
    role: '',
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSignupData({
      ...signupData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log(signupData);

    await axioss
      .post(`auth/register/`, {
        name: signupData.name,
        email: signupData.email,
        username: signupData.username,
        password: signupData.password,
        role: signupData.role,
      })
      .then((res) => {
        router.push('/login');
        // console.log(res);
        // console.log(res.data);
      })
      .catch((err) => {
        var error = '';
        for (var i in err.response.data) {
          error += i + ':' + err.response.data[i][0] + '\n';
        }
        Swal.fire({
          title: 'Error!',
          text: error,
          icon: 'error',
          confirmButtonText: 'OK',
        });
      });
  };

  var isLogin = false;

  if (typeof window !== 'undefined') {
    if (
      localStorage.getItem('role') == 1 ||
      localStorage.getItem('role') == 0
    ) {
      isLogin = true;
    }
    if (isLogin) {
      router.push('/');
    }
  }

  return (
    <OuterBox>
      <h2
        style={{ fontSize: '1.5rem', fontWeight: 'bold', textAlign: 'center' }}
      >
        Sign Up to Continue
      </h2>
      <LoginBox>
        <form onSubmit={handleSubmit}>
          <FormControl id="name" my={5} isRequired>
            <FormLabel>Name</FormLabel>
            <Input name="name" type="text" onChange={handleChange} />
          </FormControl>
          <FormControl id="email" my={5} isRequired>
            <FormLabel>Email address</FormLabel>
            <Input name="email" type="email" onChange={handleChange} />
          </FormControl>
          <FormControl id="username" my={5} isRequired>
            <FormLabel>Username</FormLabel>
            <Input name="username" type="text" onChange={handleChange} />
          </FormControl>
          <FormControl id="password" my={5} isRequired>
            <FormLabel>Password</FormLabel>
            <Input name="password" type="password" onChange={handleChange} />
          </FormControl>
          <FormControl id="role" my={5} isRequired>
            <FormLabel>Role</FormLabel>
            <Select
              name="role"
              placeholder="Select Role"
              onChange={handleChange}
            >
              <option value="0">Siswa</option>
              <option value="1">Pengajar</option>
            </Select>
          </FormControl>
          <Button my={1} width="100%" type="submit">
            Signup
          </Button>
        </form>
        <HStack my={3}>
          <Button
            leftIcon={<FcGoogle />}
            w={'full'}
            variant={'outline'}
            onClick={() =>
              signIn('google', {
                callbackUrl: `${window.location.origin}/google-signup/`,
              })
            }
          >
            <Center>Continue with Google</Center>
          </Button>
        </HStack>
        <p>
          Already have an account?{' '}
          <Link href="/login/">
            <a style={{ color: 'blue' }}>Login Here</a>
          </Link>
        </p>
      </LoginBox>
    </OuterBox>
  );
};

export default FormSignup;
