import Head from 'next/head';
import { useRouter } from 'next/router';
import { useToast } from '@chakra-ui/react';
import { useState, useEffect } from 'react';

import Layout from '../../../../../components/Layout';
import * as S from '../../../../../components/Forum/Styles';
import ThreadDetail from '../../../../../components/Forum/ThreadDetail';
import { getThread } from '../../../../../services/forum';

export default function Thread() {
  const router = useRouter();
  const toast = useToast();
  const [threadDetail, setThreadDetail] = useState(null);

  useEffect(() => {
    if (router.isReady) {
      const { forumId, threadId } = router.query;
      fetchThreadDetail(forumId, threadId);
    }
  }, [router.isReady]);

  const fetchThreadDetail = async (forumId, threadId) => {
    try {
      const threadDetailResponse = await getThread(forumId, threadId);
      setThreadDetail(threadDetailResponse.data);
    } catch {
      toast({
        title: 'Failed',
        description: 'Oops! terjadi kesalahan ketika mengambil data thread',
        status: 'error',
        duration: 5000,
        isClosable: true,
        position: 'top',
      });
    }
  };

  return (
    <>
      <Head>
        <title>
          {threadDetail?.title} | {threadDetail?.forum_name}
        </title>
      </Head>

      <Layout>
        <S.Container>
          {threadDetail && <ThreadDetail threadDetail={threadDetail} />}
        </S.Container>
      </Layout>
    </>
  );
}
