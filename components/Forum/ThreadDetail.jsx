import Link from 'next/link';
import { Heading } from '@chakra-ui/react';
import { ChevronLeftIcon } from '@chakra-ui/icons';

import * as S from '../../components/Forum/Styles';
import ThreadMainPost from './ThreadMainPost';
import ThreadReply from './ThreadReply';

const ThreadDetail = ({ threadDetail }) => {
  const {
    id: threadId,
    forum_id,
    forum_name,
    title,
    body,
    created_by_id,
    created_by,
    created_at,
    updated_at,
    replies,
  } = threadDetail;

  return (
    <S.ThreadDetail>
      <Link href={`/forum/${forum_id}`}>
        <Heading
          d="flex"
          alignItems="center"
          cursor="pointer"
          fontSize="1.5rem"
          mb="-1rem"
        >
          <ChevronLeftIcon d="inline" mr="0.25rem" color="black" />
          {forum_name}
        </Heading>
      </Link>
      <Heading>{title}</Heading>

      <ThreadMainPost
        id={threadId}
        forum_id={forum_id}
        title={title}
        body={body}
        created_by_id={created_by_id}
        created_by={created_by}
        created_at={created_at}
        updated_at={updated_at}
      />

      {!!replies?.length && (
        <S.ThreadReplies>
          {replies.map((reply, idx) => (
            <ThreadReply
              key={idx}
              threadId={threadId}
              forumId={forum_id}
              {...reply}
            />
          ))}
        </S.ThreadReplies>
      )}
    </S.ThreadDetail>
  );
};

export default ThreadDetail;
