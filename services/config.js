const config = {
  prod: 'https://dewantara.herokuapp.com/api/v1/',
  dev: 'https://develop-dewantara.herokuapp.com/api/v1/',
  local: 'http://localhost:8000/api/v1/',
};

export default config[process.env.NEXT_PUBLIC_NODE_ENV || 'local'];
