import { Button } from '@chakra-ui/button';
import { AlertDialog } from '@chakra-ui/modal';
import { AlertDialogOverlay } from '@chakra-ui/modal';
import {
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
} from '@chakra-ui/modal';
import { useRouter } from 'next/dist/client/router';
import { useState, useRef } from 'react';
import axiosInstance from '../../services/axios';
import Swal from 'sweetalert2';

export default function DeleteDialog(props) {
  const [isOpen, setIsOpen] = useState(false);
  const onClose = () => setIsOpen(false);
  const cancelRef = useRef();
  const router = useRouter();

  const handleDelete = async (e) => {
    e.preventDefault();
    await axiosInstance
      .delete(`announcement/${props.id}/`)
      .then((res) => {
        Swal.fire({
          title: 'Success',
          text: 'Berhasil Menghapus Pengumuman',
          icon: 'success',
          confirmButtonText: 'OK',
        }).then(router.push('/'));
      })
      .catch((err) => console.log(err));
  };

  return (
    <>
      <a role="button" onClick={() => setIsOpen(true)}>
        Delete
      </a>

      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isCentered
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Pengumuman
            </AlertDialogHeader>

            <AlertDialogBody>
              Anda yakin untuk menghapus pengumuman ini?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                Cancel
              </Button>
              <Button background="red" onClick={handleDelete} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
}
