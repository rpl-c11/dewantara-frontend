import Head from 'next/head';
import FormGoogleSignup from '../components/FormSignup/FormGoogleSignup';
import Layout from '../components/Layout';
import styles from '../styles/Home.module.css';

export default function GoogleSignup() {
  return (
    <Layout>
      <div className={styles.container}>
        <Head>
          <title>Google Sign Up</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <FormGoogleSignup />
      </div>
    </Layout>
  );
}
