import Link from 'next/dist/client/link';
import { Button } from '@chakra-ui/button';

export default function AddAnnouncementButton() {
  return (
    <Link href="/announcement/add">
      <Button
        bg="black"
        variant="ghost"
        color="white"
        _hover={{ bg: 'black' }}
        _focus={{ bg: 'black' }}
      >
        Tambahkan Pengumuman
      </Button>
    </Link>
  );
}
